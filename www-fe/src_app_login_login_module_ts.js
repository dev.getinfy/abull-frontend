(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_login_login_module_ts"],{

/***/ 5393:
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 6825);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 107:
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 5393);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 6825);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 6825:
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 6770);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 1339);
/* harmony import */ var _app_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../app-http.service */ 8094);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../a-bull-api.service */ 3359);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _OTP_modal_OTP_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./OTP-modal/OTP-modal.component */ 1354);







// import { ModalPage } from '../modal/modal.page';

let LoginPage = class LoginPage {
    constructor(modalController, aBullservice, http) {
        this.modalController = modalController;
        this.aBullservice = aBullservice;
        this.http = http;
    }
    ngOnInit() {
    }
    presentModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _OTP_modal_OTP_modal_component__WEBPACK_IMPORTED_MODULE_4__.OTPModalComponent,
                cssClass: 'my-custom-class'
            });
            return yield modal.present();
        });
    }
    Onlogin() {
        if (!this.phone) {
            this.http.ShowToastMessage("Enter Valid Phone Number", "Please try again later", "danger");
            return;
        }
        if (this.phone) {
            let data = {
                "phone": this.phone
            };
            this.aBullservice.aBullSignIn(data).subscribe(res => {
                if (res.status == 'success') {
                    this.http.ShowToastMessage("OTP sent successfully", " ", "success");
                }
                if (res.status == 'failed') {
                    this.http.ShowToastMessage("login failed", "Please try again later", "danger");
                }
                if (res.message == 'Otp sent') {
                    this.openMyModal();
                }
            }, err => {
                this.http.ShowToastMessage("login failed", "Please try again later", "danger");
                console.log(err);
            });
        }
    }
    openMyModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _OTP_modal_OTP_modal_component__WEBPACK_IMPORTED_MODULE_4__.OTPModalComponent,
                backdropDismiss: true,
                cssClass: 'options_modal',
                componentProps: { phone: this.phone }
            });
            return yield modal.present();
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_3__.ABullApiService },
    { type: _app_http_service__WEBPACK_IMPORTED_MODULE_2__.AppHttpService }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 1339:
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 6770:
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-item>\r\n\r\n \r\n\r\n  <ion-label>login</ion-label>\r\n</ion-item> -->\r\n<ion-header class=\"ion-no-border header-moblie\">\r\n  <ion-toolbar>\r\n    <div class=\"login-page\">\r\n      <img class=\"img-logo\" src=\"/assets/images/aBull Logo new1.png\">\r\n      <h3>Login</h3>\r\n    </div>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row class=\"ion-justify-content-evenly\">\r\n      <ion-col size=\"12\" sizeLg=\"7.5\" sizeMd=\"12\"  class=\"web-img vertical-line\">\r\n        <div class=\"web-logo\">\r\n          <img class=\"img-logo\" src=\"/assets/images/aBull Logo new1.png\">\r\n        </div>\r\n        <div class=\"ion-margin-vertical page-img\">\r\n          <img src=\"../../assets/images/Component 5 – 1@2x.png\" alt=\"\">\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"12\"  sizeLg=\"3.5\" sizeSm=\"12\" class=\"ion-align-self-center\">\r\n        <div class=\"ion-text-center\">\r\n          <h1 class=\"web-title\"> <b>User Login</b> </h1>\r\n          <ion-item class=\"input-div\" lines=\"none\">\r\n            <ion-label class=\"label-title\" position=\"stacked\">Phone Number</ion-label>\r\n            <ion-input class=\"input-box\" type=\"number\" id=\"cellNumber\" [(ngModel)]=\"phone\" #cellNumber=\"ngModel\" required=\"true\"></ion-input>\r\n          </ion-item>\r\n          <div *ngIf=\"cellNumber.invalid && cellNumber.dirty\" class=\"alert alert-danger\">\r\n\r\n            <div *ngIf=\"cellNumber.errors.required\">\r\n              <ion-text color=\"danger\">\r\n                <small> Mobile number is required.</small>\r\n              </ion-text>\r\n  \r\n            </div>\r\n  \r\n          </div>\r\n          <!-- <ion-label class=\"text-right ion-margin-bottom\" position=\"floating\">Change of Phone Number</ion-label> -->\r\n\r\n\r\n          <ion-button class=\"btn-style ion-margin\" color=\"warning\" (click)=\"Onlogin()\">Get OTP</ion-button>\r\n\r\n\r\n          <div class=\"section-border\"><h2>Or</h2></div>\r\n\r\n\r\n          <!-- <div style=\"margin-top: 40px;\">\r\n\r\n            <div class=\"flex-box\">\r\n              <div class=\"social-icons\" (click)=\"('FACEBOOK')\">\r\n                <img src=\"../../assets/images/NoPath.png\" alt=\"google\">\r\n              </div>\r\n              <div class=\"social-icons\" (click)=\"('INSTAGRAM')\">\r\n                <img src=\"../../assets/icon/instagram-color.svg\" alt=\"\">\r\n              </div>\r\n              <div class=\"social-icons\" (click)=\"('TWITTER')\">\r\n                <img src=\"../../assets/icon/facebook-color.svg\" alt=\"\">\r\n\r\n              </div>\r\n              <div class=\"social-icons\" (click)=\"('YOUTUBE')\">\r\n                <img src=\"../../assets/icon/linkedin-color.svg\" alt=\"\">\r\n              </div>\r\n            </div>\r\n          </div> -->\r\n\r\n          <div class=\"ion-text-center ion-padding text-center\">\r\n            <span>Not a User yet ?</span>\r\n            <span class=\"color-400 hand\" routerLink=\"/register\">Register Now</span>\r\n          </div>\r\n\r\n          <div class=\"ion-margin-vertical mobile-img\">\r\n            <img style=\"transform: scale(1.05);\" src=\"../../assets/images/Component 5 – 1@2x.png\" alt=\"\">\r\n          </div>\r\n        </div>\r\n      </ion-col>\r\n\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n\r\n  <ion-footer class=\"ion-border-top web-footer\">\r\n    <ion-toolbar>\r\n      <ion-title class=\"ion-text-center \" style=\"font-size: .8rem; color: #0000006B;\">Copyright &copy; aBull Technologies Private Limited 2021</ion-title>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_login_login_module_ts.js.map