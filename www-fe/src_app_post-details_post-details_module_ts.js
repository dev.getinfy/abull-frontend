(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_post-details_post-details_module_ts"],{

/***/ 7495:
/*!*************************************************************!*\
  !*** ./src/app/post-details/post-details-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PostDetailsPageRoutingModule": () => (/* binding */ PostDetailsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _post_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./post-details.page */ 3374);




const routes = [
    {
        path: '',
        component: _post_details_page__WEBPACK_IMPORTED_MODULE_0__.PostDetailsPage
    }
];
let PostDetailsPageRoutingModule = class PostDetailsPageRoutingModule {
};
PostDetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PostDetailsPageRoutingModule);



/***/ }),

/***/ 7365:
/*!*****************************************************!*\
  !*** ./src/app/post-details/post-details.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PostDetailsPageModule": () => (/* binding */ PostDetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _post_details_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./post-details-routing.module */ 7495);
/* harmony import */ var _post_details_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./post-details.page */ 3374);







let PostDetailsPageModule = class PostDetailsPageModule {
};
PostDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _post_details_routing_module__WEBPACK_IMPORTED_MODULE_0__.PostDetailsPageRoutingModule
        ],
        declarations: [_post_details_page__WEBPACK_IMPORTED_MODULE_1__.PostDetailsPage]
    })
], PostDetailsPageModule);



/***/ }),

/***/ 3374:
/*!***************************************************!*\
  !*** ./src/app/post-details/post-details.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PostDetailsPage": () => (/* binding */ PostDetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_post_details_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./post-details.page.html */ 3478);
/* harmony import */ var _post_details_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./post-details.page.scss */ 1685);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../a-bull-api.service */ 3359);






let PostDetailsPage = class PostDetailsPage {
    constructor(aBull, activatedRoute) {
        this.aBull = aBull;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.postId = this.activatedRoute.snapshot.paramMap.get('id');
        // console.log(this.postId, "postId");
        this.getPostdetails(this.postId);
    }
    getPostdetails(postId) {
        this.aBull.aBullPostDetails(this.postId).subscribe(res => {
            console.log(res, "res");
            this.postDetails = res.data;
            console.log(this.postDetails, "postDetails");
        });
    }
};
PostDetailsPage.ctorParameters = () => [
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute }
];
PostDetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-post-details',
        template: _raw_loader_post_details_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_post_details_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PostDetailsPage);



/***/ }),

/***/ 1685:
/*!*****************************************************!*\
  !*** ./src/app/post-details/post-details.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwb3N0LWRldGFpbHMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ 3478:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/post-details/post-details.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-icon slot=\"start\" name=\"chevron-back-outline\" class=\"\"  routerLink=\"/app/tabs/home\"></ion-icon>\r\n    <ion-title class=\"ion-text-center\">\r\n      Post Details\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"12\" sizeLg=\"9\"  class=\"ion-align-self-center\">\r\n        <div class=\"content-center-w\">\r\n          <!-- <h1> No Posts Available.</h1> -->\r\n        </div>\r\n        </ion-col>\r\n      <ion-col size=\"12\" sizeLg=\"9\"  class=\"ion-align-self-center\">\r\n        <div class=\"content-center-w\">\r\n          <ng-container>\r\n            <ion-item lines=\"none\" style=\"margin-top: 15px;\">\r\n              <ion-avatar slot=\"start\">\r\n                <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n              </ion-avatar>\r\n              <ion-label style=\"padding: 0 0.5rem;\">\r\n                <h3>{{postDetails?.Moderator?.first_name}} {{postDetails?.Moderator?.last_name}}</h3>\r\n                <p><img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                </p>\r\n              </ion-label>\r\n              <div class=\"ion-text-right\" style=\"color: #777777; font-size: .8rem;\">\r\n                <ul>\r\n                  <li>{{postDetails?.createdAt | date:\"mediumDate\"}}</li>\r\n                </ul>\r\n    \r\n              </div>\r\n    \r\n            </ion-item>\r\n            <div class=\"page-info home-content\">\r\n              <div class=\"ion-padding text-content\">\r\n                <h1>{{postDetails?.title}}</h1>\r\n                <div [innerHtml]=\"postDetails.description\" *ngIf=\"postDetails?.description\">\r\n    \r\n                </div>\r\n    \r\n              </div>\r\n    \r\n    \r\n              <div class=\"banner-img\" *ngIf=\"postDetails?.file\">\r\n                <img src=\"{{postDetails.file}}\" alt=\"\">\r\n              </div>\r\n    \r\n    \r\n              <!-- <ion-footer class=\"ion-no-border ion-padding\">\r\n                <div class=\"bottom-align-item\">\r\n                  <ion-button color=\"primary\">\r\n                    <img src=\"../../assets/icon/ic_thumb_up_24px-1.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n                  </ion-button>\r\n    \r\n    \r\n                  <ion-button color=\"success\">\r\n                    <img src=\"../../assets/icon/Component 17 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n                  </ion-button>\r\n    \r\n                  <ion-button color=\"danger\">\r\n                    <img src=\"../../assets/icon/Path 58-1.svg\" alt=\"\">&nbsp;<span>Share</span>\r\n                  </ion-button>\r\n                </div>\r\n    \r\n    \r\n    \r\n    \r\n              </ion-footer> -->\r\n            </div>\r\n    \r\n          </ng-container>\r\n    \r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n \r\n\r\n  <!-- comment section -->\r\n  <!-- <ion-item>\r\n    <ion-label>24 comments</ion-label>\r\n    <ion-label slot=\"end\">Most relevant <img src=\"../../assets/icon/Component 12 – 2.svg\" alt=\"\"></ion-label>\r\n  </ion-item> -->\r\n\r\n  <!-- comment card - 1-->\r\n  <!-- <ion-card>\r\n    <ion-item>\r\n      <ion-avatar slot=\"start\">\r\n        <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n      </ion-avatar>\r\n      &nbsp;\r\n      <ion-label>\r\n        <h3>User @ name</h3>\r\n      </ion-label>\r\n      <ion-label>\r\n        <p> <span> &#8226;</span> 20 mins ago</p>\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-card-content>\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n    </ion-card-content>\r\n    <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Enter any notes here...\"> </ion-textarea>\r\n\r\n    <ion-footer>\r\n      <ion-row>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"primary\">\r\n            <img src=\"../../assets/icon/ic_thumb_up_24px-1.svg\" alt=\"\"> &nbsp;<div>203 &nbsp;Likes </div>\r\n          </ion-button>\r\n        </ion-col>\r\n\r\n        <ion-col center text-center>\r\n          <ion-button color=\"success\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/ic_reply_all_24px-1.svg\" alt=\"outline\"> &nbsp;<div>02 Replies </div>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-footer>\r\n  </ion-card>\r\n\r\n  <br> -->\r\n  <!-- comment -card -2 -->\r\n  <!-- <ion-card>\r\n    <ion-item>\r\n      <ion-avatar slot=\"start\">\r\n        <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n      </ion-avatar>\r\n      &nbsp;\r\n      <ion-label>\r\n        <h3>User @ name</h3>\r\n      </ion-label>\r\n      <ion-label>\r\n        <p> <span> &#8226;</span> 20 mins ago</p>\r\n      </ion-label>\r\n    </ion-item>\r\n    <ion-card-content>\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n    </ion-card-content>\r\n    <ion-footer>\r\n      <ion-row>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"primary\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\"> &nbsp;<div>203 &nbsp;Likes </div>\r\n          </ion-button>\r\n        </ion-col>\r\n\r\n        <ion-col center text-center>\r\n          <ion-button color=\"success\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/ic_reply_all_24px-1.svg\" alt=\"outline\"> &nbsp;<div>02 Replies </div>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-footer>\r\n  </ion-card> -->\r\n\r\n\r\n</ion-content>\r\n  <!-- comment flyer card -->\r\n<!-- <ion-footer>\r\n  <ion-card>\r\n    <ion-item>\r\n      <ion-avatar slot=\"start\">\r\n        <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n      </ion-avatar>\r\n      <ion-label>\r\n        <ion-input placeholder=\"write comment\"></ion-input>\r\n      </ion-label>\r\n      <ion-label> <img src=\"../../assets/icon/Component 15 – 1.svg\" alt=\"\"> </ion-label>\r\n    </ion-item>\r\n  </ion-card>\r\n\r\n</ion-footer> -->");

/***/ })

}]);
//# sourceMappingURL=src_app_post-details_post-details_module_ts.js.map