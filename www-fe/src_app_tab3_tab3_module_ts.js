(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_tab3_tab3_module_ts"],{

/***/ 9818:
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3PageRoutingModule": () => (/* binding */ Tab3PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab3.page */ 8592);




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page,
    }
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ 3746:
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3PageModule": () => (/* binding */ Tab3PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab3.page */ 8592);
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../explore-container/explore-container.module */ 581);
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab3-routing.module */ 9818);









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__.ExploreContainerComponentModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_2__.Tab3PageRoutingModule,
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page]
    })
], Tab3PageModule);



/***/ }),

/***/ 8592:
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3Page": () => (/* binding */ Tab3Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./tab3.page.html */ 4255);
/* harmony import */ var _tab3_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab3.page.scss */ 943);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../a-bull-api.service */ 3359);
/* harmony import */ var _profile_terms_terms_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../profile/terms/terms.component */ 6813);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _app_http_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app-http.service */ 8094);








// import 'capacitor-razorpay';
// import { Plugins } from '@capacitor/core';

let Tab3Page = class Tab3Page {
    constructor(aBUllservice, Acroute, http, router, modalController) {
        this.aBUllservice = aBUllservice;
        this.Acroute = Acroute;
        this.http = http;
        this.router = router;
        this.modalController = modalController;
        this.enableEnroll = false;
        this.Acroute.params.subscribe(params => {
            this.resetVals();
        });
    }
    ngOnInit() {
        this.getaBUllGTCDetails();
        this.disablePayment();
        // console.log(Checkout, "____________")
    }
    loadCheckout() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.contestDetails && !this.contestDetails.price) {
                console.warn("amount unavailable");
                return false;
            }
            const options = {
                key: 'rzp_live_ro1JaoBHiT5gz1',
                amount: this.contestDetails.price * 100,
                description: 'Credits towards abull',
                image: 'https://abull.app/assets/images/aBull%20Logo%20new1.png',
                currency: 'INR',
                name: 'Golden Trident Contest',
                // prefill: {
                //   contact: '99999999999',
                //   name: 'Razorpay Software'
                // },
                theme: {
                    color: '#3b5998'
                }
            };
            try {
                var userNumber = JSON.parse(localStorage.loggedInUser).phone_number;
                if (userNumber) {
                    options["prefill"] = {
                        contact: userNumber
                    };
                }
            }
            catch (error) {
                console.warn(error, "phone failed");
            }
            try {
                // let data = (await Checkout.open(options));
                // console.log(data.response);
                // if (data.response && data.response.razorpay_payment_id) {
                //   this.aBUllGTCRegistration(data.response.razorpay_payment_id);
                // }
            }
            catch (error) {
                this.http.ShowToastMessage("Payment failed", "Please try again later", "danger");
                console.log(error.message, "payment failed"); //Doesn't appear at all
            }
        });
    }
    aBUllGTCRegistration(payment_id) {
        try {
            this.details = JSON.parse(localStorage.getItem('gtsdetails'));
            console.log(this.details);
            if (this.details) {
                this.details["transaction_details"]["trans_id"] = payment_id;
                this.aBUllservice.aBUllGTCRegistration(this.details).subscribe(res => {
                    this.http.ShowToastMessage("You have registered successfully", " ", "success");
                    this.router.navigate(["/paysuccess"]);
                });
            }
        }
        catch (error) {
            this.http.ShowToastMessage("Registration  failed", "Please contact admin", "danger");
        }
    }
    resetVals() {
        this.errorMsg = '';
        this.edu_institution = '';
        this.age_group = '';
        this.referral_code = '';
        this.terms = false;
        this.state = '';
        this.city = '';
    }
    ngOnDestroy() {
        this.disablePayment();
    }
    validate() {
        this.disablePayment();
        this.errorMsg = '';
        if (!this.edu_institution) {
            this.errorMsg = 'please provide education institition';
            return;
        }
        if (!this.age_group) {
            this.errorMsg = 'please provide age group';
            return;
        }
        if (!this.referral_code) {
            this.errorMsg = 'please provide referal code';
            return;
        }
        if (!this.terms) {
            this.errorMsg = 'please accept our terms and condotions';
            return;
        }
        this.isEnabled = true;
        this.errorMsg = '';
        this.enablePayment();
        let data = {
            "gtc_id": this.contestDetails.id,
            "edu_institution": this.edu_institution,
            "age_group": this.age_group,
            "city": this.city,
            "state": this.state,
            "referral_code": this.referral_code,
            "is_terms_accepted": 1,
            "transaction_details": {
                "trans_status": "Success"
            }
        };
        localStorage.setItem('gtsdetails', JSON.stringify(data));
    }
    disablePayment() {
        this.enableEnroll = false;
        // $("#payment-btn-frame").addClass("invisible");
    }
    enablePayment() {
        this.enableEnroll = true;
        // $("#payment-btn-frame").removeClass("invisible")
    }
    getaBUllGTCDetails() {
        this.aBUllservice.aBullContestDetails().subscribe(res => {
            if (res && res.data) {
                this.contestDetails = res.data[0];
            }
            console.log(res, "details");
        });
    }
    // pop up T&C
    openMyModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _profile_terms_terms_component__WEBPACK_IMPORTED_MODULE_3__.TermsComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
};
Tab3Page.ctorParameters = () => [
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _app_http_service__WEBPACK_IMPORTED_MODULE_4__.AppHttpService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController }
];
Tab3Page = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-tab3',
        template: _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_tab3_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], Tab3Page);



/***/ }),

/***/ 943:
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0YWIzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 4255:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"header-moblie\">\r\n  <ion-toolbar>\r\n    <!-- <ion-icon name=\"chevron-back-outline\" class=\"back-icon\" style=\"position: unset;\" (click)=\"isEnrolled=''\"></ion-icon> -->\r\n    <ion-title class=\"ion-text-center\">\r\n      Golden Trident Contest\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n\r\n<div class=\"golden-mobile\">\r\n  <img src=\"../../assets/images/Component 16 – 1.png\" alt=\"\">\r\n</div>\r\n\r\n<ion-content class=\"secondary \">\r\n\r\n\r\n  <h1 class=\"ion-text-left ion-padding title-re-m\" >\r\n    Golden Trident Contest\r\n  </h1>\r\n  <ion-grid>\r\n    <ion-row class=\"flex-re\">\r\n      <ion-col size=\"12\" sizeLg=\"5\" class=\"ion-justify-item-center\">\r\n\r\n        <div class=\"ion-text-center bg-color scroll-d golden-input-web\" color=\"secondary\">\r\n\r\n\r\n          <div class=\"edit-input input-contest\" color=\"secondary\">\r\n            <h5>Enter your details</h5>\r\n\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Educational Institution <sup\r\n                  style=\"color: #fff;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-input color=\"light\" (keyup)=\"validate()\" [(ngModel)]=\"edu_institution\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Age Group <sup\r\n                  style=\"color: #fff;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-select [(ngModel)]=\"age_group\" value=\"brown\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n                <ion-select-option value=\"Group 1\">Group1 -(classes 7 to 12)</ion-select-option>\r\n                <ion-select-option value=\"Group 2\">Group2 -(classes 12 to post graduation)</ion-select-option>\r\n                <ion-select-option value=\"Group 3\">Group3 -(above post graduation)</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">City</ion-label>\r\n              <ion-input [(ngModel)]=\"city\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">State</ion-label>\r\n              <ion-input [(ngModel)]=\"state\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Referal Code <sup\r\n                  style=\"color: #fff;font-size: 20px; top:0;\">*</sup></ion-label>\r\n              <ion-input [(ngModel)]=\"referral_code\" (keyup)=\"validate()\"></ion-input>\r\n            </ion-item>\r\n          </div>\r\n\r\n\r\n\r\n\r\n          <div class=\"check-box-golden ion-margin-top\">\r\n            <ion-checkbox class=\"ion-check\" (ionChange)=\"validate()\" style=\"width: 15px; height: 15px;\"\r\n              [(ngModel)]=\"terms\">\r\n            </ion-checkbox> <span>I Accept <u style=\"text-underline-position: under;\"><a style=\"color: #fff;\" href=\"!#\">Terms & Conditions</a></u> by\r\n              aBull.</span>\r\n          </div>\r\n          <div *ngIf=\"errorMsg\" style=\"color: red;\">{{errorMsg}}</div>\r\n          <div class=\"ion-text-center ion-margin\" style=\"margin-bottom: 40px;\">\r\n            <div class=\"ion-text-center ion-margin\">\r\n \r\n              <ion-button class=\"btn-style\" color=\"warning\" [disabled]=\"!enableEnroll\" (click)=\"loadCheckout()\">Enroll Now</ion-button> \r\n            </div>\r\n\r\n            <ion-card style=\"margin-top: 70px;\">\r\n              <ion-card-content (click)=\"openMyModal()\">\r\n                <b>Terms & consitions, Privacy Policy, Refund & Cancellation Policy</b>\r\n              </ion-card-content>\r\n            </ion-card>\r\n\r\n          </div>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"12\" sizeLg=\"7\">\r\n        <div class=\"g-center-web\">\r\n          <div class=\"golden-webimg\">\r\n            <img src=\"../../assets/images/Component 16 – 1@3x.png\" alt=\"\">\r\n          </div>\r\n          <div class=\"flex-web\">\r\n\r\n            <div class=\"img-card\">\r\n\r\n              <h1 class=\"ion-text-center t-mobile\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title1}}</h1>\r\n\r\n              <img src=\"{{contestDetails?.url1}}\" alt=\"\">\r\n              <div>\r\n                <h1 class=\" t-web\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title1}}</h1>\r\n                <!-- <ion-title class=\"t-web\"> Golden Trident Contest Section Title 1</ion-title> -->\r\n                <p>{{contestDetails?.description1}}</p>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"img-card\">\r\n              <h1 class=\"ion-text-center t-mobile\" style=\"font-size: 1.1rem;\">{{contestDetails?.title2}}</h1>\r\n\r\n              <img src=\"{{contestDetails?.url2}}\" alt=\"\">\r\n              <div>\r\n                <h1 class=\" t-web\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title2}}</h1>\r\n                <!-- <ion-title class=\"t-web\"> Golden Trident Contest Section Title 2</ion-title> -->\r\n                <p>{{contestDetails?.description2}}</p>\r\n              </div>\r\n\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_tab3_tab3_module_ts.js.map