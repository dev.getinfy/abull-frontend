(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_register_register_module_ts"],{

/***/ 1880:
/*!*****************************************************!*\
  !*** ./src/app/register/register-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPageRoutingModule": () => (/* binding */ RegisterPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register.page */ 8135);




const routes = [
    {
        path: '',
        component: _register_page__WEBPACK_IMPORTED_MODULE_0__.RegisterPage
    }
];
let RegisterPageRoutingModule = class RegisterPageRoutingModule {
};
RegisterPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], RegisterPageRoutingModule);



/***/ }),

/***/ 8723:
/*!*********************************************!*\
  !*** ./src/app/register/register.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPageModule": () => (/* binding */ RegisterPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _register_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./register-routing.module */ 1880);
/* harmony import */ var _register_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.page */ 8135);







let RegisterPageModule = class RegisterPageModule {
};
RegisterPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _register_routing_module__WEBPACK_IMPORTED_MODULE_0__.RegisterPageRoutingModule
        ],
        declarations: [_register_page__WEBPACK_IMPORTED_MODULE_1__.RegisterPage]
    })
], RegisterPageModule);



/***/ }),

/***/ 8135:
/*!*******************************************!*\
  !*** ./src/app/register/register.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegisterPage": () => (/* binding */ RegisterPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./register.page.html */ 9200);
/* harmony import */ var _register_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./register.page.scss */ 9728);
/* harmony import */ var _app_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../app-http.service */ 8094);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../a-bull-api.service */ 3359);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _login_OTP_modal_OTP_modal_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../login/OTP-modal/OTP-modal.component */ 1354);







// import { ModalPage } from '../modal/modal.page';

let RegisterPage = class RegisterPage {
    constructor(modalController, aBullApi, http) {
        this.modalController = modalController;
        this.aBullApi = aBullApi;
        this.http = http;
        this.terms = false;
        this.countries = ["India", "United States", "United Kingdom", "Australia", "United Arab Emirates", "China", "Canada", "Japan", "Germany"];
    }
    ngOnInit() {
    }
    signUp() {
        if (!this.terms) {
            this.http.ShowToastMessage("terms and conditions", "are required", "danger");
            return;
        }
        let user = {
            first_name: this.firstName,
            last_name: this.lastName,
            phone_number: this.mobileNumber,
            email: this.userEmail,
            country: this.selectedCountry,
            dob: this.dateOfBirth,
            referral_code: "CODE"
        };
        console.log(user);
        this.aBullApi.aBullSignUp(user).subscribe(data => {
            if (data.status == 'success') {
                this.http.ShowToastMessage("registered succesfully", " ", "success");
                this.openMyModal();
            }
            if (data.status == 'failed') {
                this.http.ShowToastMessage(data.message, "danger");
            }
            console.log(data);
        }, err => {
            this.http.ShowToastMessage("register failed", "Please try again later", "danger");
        });
    }
    openMyModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _login_OTP_modal_OTP_modal_component__WEBPACK_IMPORTED_MODULE_4__.OTPModalComponent,
                backdropDismiss: true,
                cssClass: 'options_modal',
                componentProps: { phone: this.mobileNumber }
            });
            return yield modal.present();
        });
    }
};
RegisterPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_3__.ABullApiService },
    { type: _app_http_service__WEBPACK_IMPORTED_MODULE_2__.AppHttpService }
];
RegisterPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-register',
        template: _raw_loader_register_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_register_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RegisterPage);



/***/ }),

/***/ 9728:
/*!*********************************************!*\
  !*** ./src/app/register/register.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWdpc3Rlci5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 9200:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header style=\"position: unset;\" class=\"header-moblie\">\r\n  <!-- <ion-icon name=\"chevron-back-outline\" class=\"back-icon\"></ion-icon> -->\r\n  <div class=\"login-page\">\r\n    <img class=\"img-logo\" src=\"/assets/images/aBull Logo new1.png\">\r\n    <h3>Register</h3>\r\n  </div>\r\n  <!-- <ion-toolbar>\r\n    <ion-title>register</ion-title>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-text-center\">\r\n\r\n  <ion-row class=\"ion-justify-content-evenly\">\r\n\r\n\r\n    <ion-col size=\"7.5\" sizeLg=\"7.5\" class=\"web-img vertical-line ion-align-self-end\">\r\n      <div class=\"web-logo\">\r\n        <img class=\"img-logo\" src=\"/assets/images/aBull Logo new1.png\">\r\n      </div>\r\n      <div class=\"ion-margin-vertical page-img\">\r\n        <img src=\"../../assets/images/Component 5 – 1@2x.png\" alt=\"\">\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" sizeLg=\"3.5\" sizeSm=\"12\" class=\"ion-align-self-start scroll-d\" style=\"height: 96vh; overflow: auto;\">\r\n      <div class=\"group-input\">\r\n        <h1 class=\"web-title\"> <b>Register</b> </h1>\r\n        <ion-item lines=\"none\" class=\"float-left\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">First Name <sup\r\n              style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup> </ion-label>\r\n          <ion-input type=\"text\" id=\"fName\" [(ngModel)]=\"firstName\" required=\"true\" #fName=\"ngModel\"> </ion-input>\r\n        </ion-item>\r\n        <div *ngIf=\"fName.invalid && fName.dirty\" class=\"alert alert-danger\">\r\n\r\n          <div *ngIf=\"fName.errors.required\">\r\n            <ion-text color=\"danger\">\r\n              <small> First name is required.</small>\r\n            </ion-text>\r\n\r\n          </div>\r\n\r\n        </div>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">Last Name</ion-label>\r\n          <ion-input type=\"text\" id=\"lName\" [(ngModel)]=\"lastName\" required=\"true\" #lName=\"ngModel\"> </ion-input>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">Phone Number <sup\r\n              style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n          <ion-input type=\"number\" id=\"cellNumber\" [(ngModel)]=\"mobileNumber\" required=\"true\" #cellNumber=\"ngModel\">\r\n          </ion-input>\r\n        </ion-item>\r\n        <div *ngIf=\"cellNumber.invalid && cellNumber.dirty\" class=\"alert alert-danger\">\r\n\r\n          <div *ngIf=\"cellNumber.errors.required\">\r\n            <ion-text color=\"danger\">\r\n              <small> Mobile number is required.</small>\r\n            </ion-text>\r\n\r\n          </div>\r\n\r\n        </div>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">Email Address <sup\r\n              style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n          <ion-input type=\"email\" pattern=\"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$\" name=\"email\"\r\n            [(ngModel)]=\"userEmail\" required=\"true\" #email=\"ngModel\">\r\n\r\n          </ion-input>\r\n        </ion-item>\r\n        <div *ngIf=\"email.invalid && email.dirty\" class=\"alert alert-danger\">\r\n\r\n          <div *ngIf=\"email.errors.required\">\r\n            <ion-text color=\"danger\">\r\n              <small> email is required.</small>\r\n            </ion-text>\r\n\r\n          </div>\r\n\r\n          <div *ngIf=\"email.errors.pattern\">\r\n            <ion-text color=\"danger\">\r\n              <small>\r\n                Please enter a valid email</small>\r\n            </ion-text>\r\n          </div>\r\n\r\n        </div>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">Country</ion-label>\r\n          <ion-select okText=\"Okay\" cancelText=\"Dismiss\" [(ngModel)]=\"selectedCountry\">\r\n            <ion-select-option *ngFor=\"let item of countries\" [value]=\"item\">{{item}} </ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"label-title\" position=\"stacked\">Date Of Birth</ion-label>\r\n          <ion-input type=\"date\" [max]=\"CurrentDate\" id=\"dOB\" [(ngModel)]=\"dateOfBirth\" required=\"true\" #dOB=\"ngModel\">\r\n          </ion-input>\r\n        </ion-item>\r\n\r\n      </div>\r\n      <div class=\"check-box ion-text-center ion-margin-top\">\r\n        <ion-checkbox style=\"width: 15px; height: 15px;\" required=\"true\" [(ngModel)]=\"terms\"></ion-checkbox> <span> By Creating an account\r\n          you agree to the <a class=\"color-400\" href=\"\">terms of use</a>and our<a class=\"color-400\" href=\"\">\r\n            privacy policy.</a></span>\r\n      </div>\r\n      <div class=\"ion-text-center\">\r\n\r\n        <ion-button class=\"btn-style\" color=\"warning\" (click)=\"signUp()\">Submit</ion-button>\r\n\r\n      </div>\r\n      <div class=\"section-border\"><h2>Or</h2></div>\r\n      <!-- <div style=\"margin-top: 40px;\">\r\n\r\n        <div class=\"flex-box\">\r\n          <div class=\"social-icons\" (click)=\"('FACEBOOK')\">\r\n            <img src=\"../../assets/images/NoPath.png\" alt=\"google\">\r\n          </div>\r\n          <div class=\"social-icons\" (click)=\"('INSTAGRAM')\">\r\n            <img src=\"../../assets/icon/instagram-color.svg\" alt=\"\">\r\n          </div>\r\n          <div class=\"social-icons\" (click)=\"('TWITTER')\">\r\n            <img src=\"../../assets/icon/facebook-color.svg\" alt=\"\">\r\n\r\n          </div>\r\n          <div class=\"social-icons\" (click)=\"('YOUTUBE')\">\r\n            <img src=\"../../assets/icon/linkedin-color.svg\" alt=\"\">\r\n          </div>\r\n        </div>\r\n      </div> -->\r\n      <div class=\"ion-text-center ion-padding text-center\">Already have an account?\r\n        <span><a class=\"color-400\" routerLink=\"/login\">Login Now</a></span>\r\n      </div>\r\n    </ion-col>\r\n\r\n\r\n  </ion-row>\r\n\r\n\r\n  <ion-footer class=\"ion-border-top\">\r\n    <ion-toolbar>\r\n      <ion-title class=\"ion-text-center \" style=\"font-size: .8rem; color: #0000006B;\">Copyright &copy;aBull Technologies Private Limited 2021\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_register_register_module_ts.js.map