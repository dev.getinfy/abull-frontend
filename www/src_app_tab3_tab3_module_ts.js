(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_tab3_tab3_module_ts"],{

/***/ 8384:
/*!****************************************************!*\
  !*** ./node_modules/@capacitor/core/dist/index.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Capacitor": () => (/* binding */ Capacitor),
/* harmony export */   "CapacitorException": () => (/* binding */ CapacitorException),
/* harmony export */   "CapacitorPlatforms": () => (/* binding */ CapacitorPlatforms),
/* harmony export */   "ExceptionCode": () => (/* binding */ ExceptionCode),
/* harmony export */   "Plugins": () => (/* binding */ Plugins),
/* harmony export */   "WebPlugin": () => (/* binding */ WebPlugin),
/* harmony export */   "WebView": () => (/* binding */ WebView),
/* harmony export */   "addPlatform": () => (/* binding */ addPlatform),
/* harmony export */   "registerPlugin": () => (/* binding */ registerPlugin),
/* harmony export */   "registerWebPlugin": () => (/* binding */ registerWebPlugin),
/* harmony export */   "setPlatform": () => (/* binding */ setPlatform)
/* harmony export */ });
/*! Capacitor: https://capacitorjs.com/ - MIT License */
const createCapacitorPlatforms = (win) => {
    const defaultPlatformMap = new Map();
    defaultPlatformMap.set('web', { name: 'web' });
    const capPlatforms = win.CapacitorPlatforms || {
        currentPlatform: { name: 'web' },
        platforms: defaultPlatformMap,
    };
    const addPlatform = (name, platform) => {
        capPlatforms.platforms.set(name, platform);
    };
    const setPlatform = (name) => {
        if (capPlatforms.platforms.has(name)) {
            capPlatforms.currentPlatform = capPlatforms.platforms.get(name);
        }
    };
    capPlatforms.addPlatform = addPlatform;
    capPlatforms.setPlatform = setPlatform;
    return capPlatforms;
};
const initPlatforms = (win) => (win.CapacitorPlatforms = createCapacitorPlatforms(win));
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */
const CapacitorPlatforms = /*#__PURE__*/ initPlatforms((typeof globalThis !== 'undefined'
    ? globalThis
    : typeof self !== 'undefined'
        ? self
        : typeof window !== 'undefined'
            ? window
            : typeof global !== 'undefined'
                ? global
                : {}));
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */
const addPlatform = CapacitorPlatforms.addPlatform;
/**
 * @deprecated Set `CapacitorCustomPlatform` on the window object prior to runtime executing in the web app instead
 */
const setPlatform = CapacitorPlatforms.setPlatform;

const legacyRegisterWebPlugin = (cap, webPlugin) => {
    var _a;
    const config = webPlugin.config;
    const Plugins = cap.Plugins;
    if (!config || !config.name) {
        // TODO: add link to upgrade guide
        throw new Error(`Capacitor WebPlugin is using the deprecated "registerWebPlugin()" function, but without the config. Please use "registerPlugin()" instead to register this web plugin."`);
    }
    // TODO: add link to upgrade guide
    console.warn(`Capacitor plugin "${config.name}" is using the deprecated "registerWebPlugin()" function`);
    if (!Plugins[config.name] || ((_a = config === null || config === void 0 ? void 0 : config.platforms) === null || _a === void 0 ? void 0 : _a.includes(cap.getPlatform()))) {
        // Add the web plugin into the plugins registry if there already isn't
        // an existing one. If it doesn't already exist, that means
        // there's no existing native implementation for it.
        // - OR -
        // If we already have a plugin registered (meaning it was defined in the native layer),
        // then we should only overwrite it if the corresponding web plugin activates on
        // a certain platform. For example: Geolocation uses the WebPlugin on Android but not iOS
        Plugins[config.name] = webPlugin;
    }
};

var ExceptionCode;
(function (ExceptionCode) {
    /**
     * API is not implemented.
     *
     * This usually means the API can't be used because it is not implemented for
     * the current platform.
     */
    ExceptionCode["Unimplemented"] = "UNIMPLEMENTED";
    /**
     * API is not available.
     *
     * This means the API can't be used right now because:
     *   - it is currently missing a prerequisite, such as network connectivity
     *   - it requires a particular platform or browser version
     */
    ExceptionCode["Unavailable"] = "UNAVAILABLE";
})(ExceptionCode || (ExceptionCode = {}));
class CapacitorException extends Error {
    constructor(message, code) {
        super(message);
        this.message = message;
        this.code = code;
    }
}
const getPlatformId = (win) => {
    var _a, _b;
    if (win === null || win === void 0 ? void 0 : win.androidBridge) {
        return 'android';
    }
    else if ((_b = (_a = win === null || win === void 0 ? void 0 : win.webkit) === null || _a === void 0 ? void 0 : _a.messageHandlers) === null || _b === void 0 ? void 0 : _b.bridge) {
        return 'ios';
    }
    else {
        return 'web';
    }
};

const createCapacitor = (win) => {
    var _a, _b, _c, _d, _e;
    const capCustomPlatform = win.CapacitorCustomPlatform || null;
    const cap = win.Capacitor || {};
    const Plugins = (cap.Plugins = cap.Plugins || {});
    /**
     * @deprecated Use `capCustomPlatform` instead, default functions like registerPlugin will function with the new object.
     */
    const capPlatforms = win.CapacitorPlatforms;
    const defaultGetPlatform = () => {
        return capCustomPlatform !== null
            ? capCustomPlatform.name
            : getPlatformId(win);
    };
    const getPlatform = ((_a = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _a === void 0 ? void 0 : _a.getPlatform) || defaultGetPlatform;
    const defaultIsNativePlatform = () => getPlatform() !== 'web';
    const isNativePlatform = ((_b = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _b === void 0 ? void 0 : _b.isNativePlatform) || defaultIsNativePlatform;
    const defaultIsPluginAvailable = (pluginName) => {
        const plugin = registeredPlugins.get(pluginName);
        if (plugin === null || plugin === void 0 ? void 0 : plugin.platforms.has(getPlatform())) {
            // JS implementation available for the current platform.
            return true;
        }
        if (getPluginHeader(pluginName)) {
            // Native implementation available.
            return true;
        }
        return false;
    };
    const isPluginAvailable = ((_c = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _c === void 0 ? void 0 : _c.isPluginAvailable) ||
        defaultIsPluginAvailable;
    const defaultGetPluginHeader = (pluginName) => { var _a; return (_a = cap.PluginHeaders) === null || _a === void 0 ? void 0 : _a.find(h => h.name === pluginName); };
    const getPluginHeader = ((_d = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _d === void 0 ? void 0 : _d.getPluginHeader) || defaultGetPluginHeader;
    const handleError = (err) => win.console.error(err);
    const pluginMethodNoop = (_target, prop, pluginName) => {
        return Promise.reject(`${pluginName} does not have an implementation of "${prop}".`);
    };
    const registeredPlugins = new Map();
    const defaultRegisterPlugin = (pluginName, jsImplementations = {}) => {
        const registeredPlugin = registeredPlugins.get(pluginName);
        if (registeredPlugin) {
            console.warn(`Capacitor plugin "${pluginName}" already registered. Cannot register plugins twice.`);
            return registeredPlugin.proxy;
        }
        const platform = getPlatform();
        const pluginHeader = getPluginHeader(pluginName);
        let jsImplementation;
        const loadPluginImplementation = async () => {
            if (!jsImplementation && platform in jsImplementations) {
                jsImplementation =
                    typeof jsImplementations[platform] === 'function'
                        ? (jsImplementation = await jsImplementations[platform]())
                        : (jsImplementation = jsImplementations[platform]);
            }
            else if (capCustomPlatform !== null &&
                !jsImplementation &&
                'web' in jsImplementations) {
                jsImplementation =
                    typeof jsImplementations['web'] === 'function'
                        ? (jsImplementation = await jsImplementations['web']())
                        : (jsImplementation = jsImplementations['web']);
            }
            return jsImplementation;
        };
        const createPluginMethod = (impl, prop) => {
            var _a, _b;
            if (pluginHeader) {
                const methodHeader = pluginHeader === null || pluginHeader === void 0 ? void 0 : pluginHeader.methods.find(m => prop === m.name);
                if (methodHeader) {
                    if (methodHeader.rtype === 'promise') {
                        return (options) => cap.nativePromise(pluginName, prop.toString(), options);
                    }
                    else {
                        return (options, callback) => cap.nativeCallback(pluginName, prop.toString(), options, callback);
                    }
                }
                else if (impl) {
                    return (_a = impl[prop]) === null || _a === void 0 ? void 0 : _a.bind(impl);
                }
            }
            else if (impl) {
                return (_b = impl[prop]) === null || _b === void 0 ? void 0 : _b.bind(impl);
            }
            else {
                throw new CapacitorException(`"${pluginName}" plugin is not implemented on ${platform}`, ExceptionCode.Unimplemented);
            }
        };
        const createPluginMethodWrapper = (prop) => {
            let remove;
            const wrapper = (...args) => {
                const p = loadPluginImplementation().then(impl => {
                    const fn = createPluginMethod(impl, prop);
                    if (fn) {
                        const p = fn(...args);
                        remove = p === null || p === void 0 ? void 0 : p.remove;
                        return p;
                    }
                    else {
                        throw new CapacitorException(`"${pluginName}.${prop}()" is not implemented on ${platform}`, ExceptionCode.Unimplemented);
                    }
                });
                if (prop === 'addListener') {
                    p.remove = async () => remove();
                }
                return p;
            };
            // Some flair ✨
            wrapper.toString = () => `${prop.toString()}() { [capacitor code] }`;
            Object.defineProperty(wrapper, 'name', {
                value: prop,
                writable: false,
                configurable: false,
            });
            return wrapper;
        };
        const addListener = createPluginMethodWrapper('addListener');
        const removeListener = createPluginMethodWrapper('removeListener');
        const addListenerNative = (eventName, callback) => {
            const call = addListener({ eventName }, callback);
            const remove = async () => {
                const callbackId = await call;
                removeListener({
                    eventName,
                    callbackId,
                }, callback);
            };
            const p = new Promise(resolve => call.then(() => resolve({ remove })));
            p.remove = async () => {
                console.warn(`Using addListener() without 'await' is deprecated.`);
                await remove();
            };
            return p;
        };
        const proxy = new Proxy({}, {
            get(_, prop) {
                switch (prop) {
                    // https://github.com/facebook/react/issues/20030
                    case '$$typeof':
                        return undefined;
                    case 'toJSON':
                        return () => ({});
                    case 'addListener':
                        return pluginHeader ? addListenerNative : addListener;
                    case 'removeListener':
                        return removeListener;
                    default:
                        return createPluginMethodWrapper(prop);
                }
            },
        });
        Plugins[pluginName] = proxy;
        registeredPlugins.set(pluginName, {
            name: pluginName,
            proxy,
            platforms: new Set([
                ...Object.keys(jsImplementations),
                ...(pluginHeader ? [platform] : []),
            ]),
        });
        return proxy;
    };
    const registerPlugin = ((_e = capPlatforms === null || capPlatforms === void 0 ? void 0 : capPlatforms.currentPlatform) === null || _e === void 0 ? void 0 : _e.registerPlugin) || defaultRegisterPlugin;
    // Add in convertFileSrc for web, it will already be available in native context
    if (!cap.convertFileSrc) {
        cap.convertFileSrc = filePath => filePath;
    }
    cap.getPlatform = getPlatform;
    cap.handleError = handleError;
    cap.isNativePlatform = isNativePlatform;
    cap.isPluginAvailable = isPluginAvailable;
    cap.pluginMethodNoop = pluginMethodNoop;
    cap.registerPlugin = registerPlugin;
    cap.Exception = CapacitorException;
    cap.DEBUG = !!cap.DEBUG;
    cap.isLoggingEnabled = !!cap.isLoggingEnabled;
    // Deprecated props
    cap.platform = cap.getPlatform();
    cap.isNative = cap.isNativePlatform();
    return cap;
};
const initCapacitorGlobal = (win) => (win.Capacitor = createCapacitor(win));

const Capacitor = /*#__PURE__*/ initCapacitorGlobal(typeof globalThis !== 'undefined'
    ? globalThis
    : typeof self !== 'undefined'
        ? self
        : typeof window !== 'undefined'
            ? window
            : typeof global !== 'undefined'
                ? global
                : {});
const registerPlugin = Capacitor.registerPlugin;
/**
 * @deprecated Provided for backwards compatibility for Capacitor v2 plugins.
 * Capacitor v3 plugins should import the plugin directly. This "Plugins"
 * export is deprecated in v3, and will be removed in v4.
 */
const Plugins = Capacitor.Plugins;
/**
 * Provided for backwards compatibility. Use the registerPlugin() API
 * instead, and provide the web plugin as the "web" implmenetation.
 * For example
 *
 * export const Example = registerPlugin('Example', {
 *   web: () => import('./web').then(m => new m.Example())
 * })
 *
 * @deprecated Deprecated in v3, will be removed from v4.
 */
const registerWebPlugin = (plugin) => legacyRegisterWebPlugin(Capacitor, plugin);

/**
 * Base class web plugins should extend.
 */
class WebPlugin {
    constructor(config) {
        this.listeners = {};
        this.windowListeners = {};
        if (config) {
            // TODO: add link to upgrade guide
            console.warn(`Capacitor WebPlugin "${config.name}" config object was deprecated in v3 and will be removed in v4.`);
            this.config = config;
        }
    }
    addListener(eventName, listenerFunc) {
        const listeners = this.listeners[eventName];
        if (!listeners) {
            this.listeners[eventName] = [];
        }
        this.listeners[eventName].push(listenerFunc);
        // If we haven't added a window listener for this event and it requires one,
        // go ahead and add it
        const windowListener = this.windowListeners[eventName];
        if (windowListener && !windowListener.registered) {
            this.addWindowListener(windowListener);
        }
        const remove = async () => this.removeListener(eventName, listenerFunc);
        const p = Promise.resolve({ remove });
        Object.defineProperty(p, 'remove', {
            value: async () => {
                console.warn(`Using addListener() without 'await' is deprecated.`);
                await remove();
            },
        });
        return p;
    }
    async removeAllListeners() {
        this.listeners = {};
        for (const listener in this.windowListeners) {
            this.removeWindowListener(this.windowListeners[listener]);
        }
        this.windowListeners = {};
    }
    notifyListeners(eventName, data) {
        const listeners = this.listeners[eventName];
        if (listeners) {
            listeners.forEach(listener => listener(data));
        }
    }
    hasListeners(eventName) {
        return !!this.listeners[eventName].length;
    }
    registerWindowListener(windowEventName, pluginEventName) {
        this.windowListeners[pluginEventName] = {
            registered: false,
            windowEventName,
            pluginEventName,
            handler: event => {
                this.notifyListeners(pluginEventName, event);
            },
        };
    }
    unimplemented(msg = 'not implemented') {
        return new Capacitor.Exception(msg, ExceptionCode.Unimplemented);
    }
    unavailable(msg = 'not available') {
        return new Capacitor.Exception(msg, ExceptionCode.Unavailable);
    }
    async removeListener(eventName, listenerFunc) {
        const listeners = this.listeners[eventName];
        if (!listeners) {
            return;
        }
        const index = listeners.indexOf(listenerFunc);
        this.listeners[eventName].splice(index, 1);
        // If there are no more listeners for this type of event,
        // remove the window listener
        if (!this.listeners[eventName].length) {
            this.removeWindowListener(this.windowListeners[eventName]);
        }
    }
    addWindowListener(handle) {
        window.addEventListener(handle.windowEventName, handle.handler);
        handle.registered = true;
    }
    removeWindowListener(handle) {
        if (!handle) {
            return;
        }
        window.removeEventListener(handle.windowEventName, handle.handler);
        handle.registered = false;
    }
}

const WebView = /*#__PURE__*/ registerPlugin('WebView');


//# sourceMappingURL=index.js.map


/***/ }),

/***/ 4192:
/*!***********************************************************!*\
  !*** ./node_modules/capacitor-razorpay/dist/esm/index.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Checkout": () => (/* reexport safe */ _web__WEBPACK_IMPORTED_MODULE_0__.Checkout),
/* harmony export */   "CheckoutWeb": () => (/* reexport safe */ _web__WEBPACK_IMPORTED_MODULE_0__.CheckoutWeb)
/* harmony export */ });
/* harmony import */ var _web__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./web */ 4307);

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 4307:
/*!*********************************************************!*\
  !*** ./node_modules/capacitor-razorpay/dist/esm/web.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CheckoutWeb": () => (/* binding */ CheckoutWeb),
/* harmony export */   "Checkout": () => (/* binding */ Checkout)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 8384);

class CheckoutWeb extends _capacitor_core__WEBPACK_IMPORTED_MODULE_0__.WebPlugin {
    constructor() {
        super({
            name: 'Checkout',
            platforms: ['web'],
        });
    }
    async echo(options) {
        console.log('ECHO', options);
        return options;
    }
    async open(options) {
        console.log(options);
        return new Promise((resolve, reject) => {
            var _a;
            var razorpay;
            //add handlers to options to get the success response
            var finalOps = options;
            finalOps['handler'] = function (res) {
                console.log(res.razorpay_payment_id);
                resolve({
                    response: res
                });
            };
            finalOps['modal.ondismiss'] = function () {
                reject(JSON.stringify({ code: 2, description: 'Payment Canceled by User' }));
            };
            var retryCount = 0;
            if (finalOps.hasOwnProperty('retry')) {
                if (finalOps.retry.enabled === true) {
                    retryCount = finalOps.retry.max_count;
                }
            }
            //get the first script tag in the document
            var rjs = document.getElementsByTagName('script')[0];
            //create a HTMLScriptElement object for rzp script injection
            var rzpjs = document.createElement('script');
            rzpjs.id = 'rzp-jssdk';
            rzpjs.setAttribute('src', 'https://checkout.razorpay.com/v1/checkout.js');
            (_a = rjs.parentNode) === null || _a === void 0 ? void 0 : _a.appendChild(rzpjs);
            rzpjs.addEventListener('load', () => {
                try {
                    razorpay = new window.Razorpay(finalOps);
                    razorpay.open();
                    razorpay.on(('payment.failed'), (res) => {
                        var _a;
                        retryCount = retryCount - 1;
                        if (retryCount < 0) {
                            console.log(res);
                            (_a = rjs.parentNode) === null || _a === void 0 ? void 0 : _a.removeChild(rzpjs);
                            reject(res.error);
                        }
                    });
                }
                catch (err) {
                    reject({
                        response: err
                    });
                }
            });
        });
        // var rjs = document.getElementsByTagName('script')[0]
        // var razorpay;
        // var js:HTMLScriptElement = document.createElement('script')
        // js.id = 'rzp-jssdk'
        // js.setAttribute('src','https://checkout.razorpay.com/v1/checkout.js')
        // document.body.appendChild(js);
        // document.body.appendChild(js);
        // rjs.parentNode?.appendChild(js)
        // rjs.addEventListener('load',()=>{
        //   try{
        //     razorpay = new (window as any).Razorpay(options)
        //     razorpay.open()
        //     razorpay.on('payment.failed',function(response:any){
        //         console.log('payment failed')
        //         console.log(response)
        //     })
        //   }catch(err){
        //     document.body.removeChild(js)
        //     return{
        //       response: "Problem opening checkouts"
        //     }
        //   }
        // })
        // return {
        //   response:options.key
        // }
    }
}
const Checkout = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Checkout', {
    web: () => Promise.resolve(/*! import() */).then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 4307)).then(m => new m.CheckoutWeb())
});

//# sourceMappingURL=web.js.map

/***/ }),

/***/ 9818:
/*!*********************************************!*\
  !*** ./src/app/tab3/tab3-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3PageRoutingModule": () => (/* binding */ Tab3PageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab3.page */ 8592);




const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page,
    },
    { path: 'contest', loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_contest_contest_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ../contest/contest.module */ 1578)).then(m => m.ContestModule) },
];
let Tab3PageRoutingModule = class Tab3PageRoutingModule {
};
Tab3PageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], Tab3PageRoutingModule);



/***/ }),

/***/ 3746:
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3PageModule": () => (/* binding */ Tab3PageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tab3.page */ 8592);
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../explore-container/explore-container.module */ 581);
/* harmony import */ var _tab3_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab3-routing.module */ 9818);









let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_1__.ExploreContainerComponentModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule.forChild([{ path: '', component: _tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page }]),
            _tab3_routing_module__WEBPACK_IMPORTED_MODULE_2__.Tab3PageRoutingModule,
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_0__.Tab3Page]
    })
], Tab3PageModule);



/***/ }),

/***/ 8592:
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Tab3Page": () => (/* binding */ Tab3Page)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./tab3.page.html */ 4255);
/* harmony import */ var _tab3_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tab3.page.scss */ 943);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../a-bull-api.service */ 3359);
/* harmony import */ var _profile_terms_terms_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../profile/terms/terms.component */ 6813);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _profile_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../profile/privacy/privacy.component */ 8159);
/* harmony import */ var _profile_refund_refund_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../profile/refund/refund.component */ 6173);
/* harmony import */ var capacitor_razorpay__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! capacitor-razorpay */ 4192);
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/core */ 8384);
/* harmony import */ var _app_http_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../app-http.service */ 8094);













const { Checkout } = _capacitor_core__WEBPACK_IMPORTED_MODULE_7__.Plugins;
let Tab3Page = class Tab3Page {
    constructor(aBUllservice, Acroute, http, router, modalController) {
        this.aBUllservice = aBUllservice;
        this.Acroute = Acroute;
        this.http = http;
        this.router = router;
        this.modalController = modalController;
        this.enableEnroll = false;
        this.Acroute.params.subscribe(params => {
            this.resetVals();
        });
    }
    ngOnInit() {
        this.getaBUllGTCDetails();
        this.disablePayment();
        setTimeout(() => {
            this.router.navigate(['app/tabs/gtc/contest']);
        }, 2000);
        // console.log(Checkout, "____________")
    }
    loadCheckout() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.contestDetails && !this.contestDetails.price) {
                console.warn("amount unavailable");
                return false;
            }
            const options = {
                key: 'rzp_live_ro1JaoBHiT5gz1',
                amount: this.contestDetails.price * 100,
                description: 'Credits towards abull',
                image: 'https://abull.app/assets/images/aBull%20Logo%20new1.png',
                currency: 'INR',
                name: 'Golden Trident Contest',
                // prefill: {
                //   contact: '99999999999',
                //   name: 'Razorpay Software'
                // },
                theme: {
                    color: '#3b5998'
                }
            };
            try {
                var userNumber = JSON.parse(localStorage.loggedInUser).phone_number;
                if (userNumber) {
                    options["prefill"] = {
                        contact: userNumber
                    };
                }
            }
            catch (error) {
                console.warn(error, "phone failed");
            }
            try {
                let data = (yield Checkout.open(options));
                console.log(data.response);
                if (data.response && data.response.razorpay_payment_id) {
                    this.aBUllGTCRegistration(data.response.razorpay_payment_id);
                }
            }
            catch (error) {
                this.http.ShowToastMessage("Payment failed", "Please try again later", "danger");
                console.log(error.message, "payment failed"); //Doesn't appear at all
            }
        });
    }
    aBUllGTCRegistration(payment_id) {
        try {
            this.details = JSON.parse(localStorage.getItem('gtsdetails'));
            console.log(this.details);
            if (this.details) {
                this.details["transaction_details"]["trans_id"] = payment_id;
                this.aBUllservice.aBUllGTCRegistration(this.details).subscribe(res => {
                    this.http.ShowToastMessage("You have registered successfully", " ", "success");
                    this.router.navigate(["/paysuccess"]);
                });
            }
        }
        catch (error) {
            this.http.ShowToastMessage("Registration  failed", "Please contact admin", "danger");
        }
    }
    resetVals() {
        this.errorMsg = '';
        this.edu_institution = '';
        this.age_group = '';
        this.referral_code = '';
        this.terms = false;
        this.state = '';
        this.city = '';
    }
    ngOnDestroy() {
        this.disablePayment();
    }
    validate() {
        this.disablePayment();
        this.errorMsg = '';
        if (!this.edu_institution) {
            this.errorMsg = 'please provide education institition';
            return;
        }
        if (!this.age_group) {
            this.errorMsg = 'please provide age group';
            return;
        }
        if (!this.referral_code) {
            this.errorMsg = 'please provide referal code';
            return;
        }
        if (!this.terms) {
            this.errorMsg = 'please accept our terms and condotions';
            return;
        }
        this.isEnabled = true;
        this.errorMsg = '';
        this.enablePayment();
        let data = {
            "gtc_id": this.contestDetails.id,
            "edu_institution": this.edu_institution,
            "age_group": this.age_group,
            "city": this.city,
            "state": this.state,
            "referral_code": this.referral_code,
            "is_terms_accepted": 1,
            "transaction_details": {
                "trans_status": "Success"
            }
        };
        localStorage.setItem('gtsdetails', JSON.stringify(data));
    }
    disablePayment() {
        this.enableEnroll = false;
        // $("#payment-btn-frame").addClass("invisible");
    }
    enablePayment() {
        this.enableEnroll = true;
        // $("#payment-btn-frame").removeClass("invisible")
    }
    getaBUllGTCDetails() {
        this.aBUllservice.aBullContestDetails().subscribe(res => {
            if (res && res.data) {
                this.contestDetails = res.data[0];
            }
            console.log(res, "details");
        });
    }
    // pop up T&C
    openMyTerms() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _profile_terms_terms_component__WEBPACK_IMPORTED_MODULE_3__.TermsComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    openMyPrivacy() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _profile_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_4__.PrivacyComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    openMyRefund() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _profile_refund_refund_component__WEBPACK_IMPORTED_MODULE_5__.RefundComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
};
Tab3Page.ctorParameters = () => [
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.ActivatedRoute },
    { type: _app_http_service__WEBPACK_IMPORTED_MODULE_8__.AppHttpService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.ModalController }
];
Tab3Page = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-tab3',
        template: _raw_loader_tab3_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_tab3_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], Tab3Page);



/***/ }),

/***/ 943:
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0YWIzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 4255:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"header-moblie\">\r\n  <ion-toolbar>\r\n    <!-- <ion-icon name=\"chevron-back-outline\" class=\"back-icon\" style=\"position: unset;\" (click)=\"isEnrolled=''\"></ion-icon> -->\r\n    <ion-title class=\"ion-text-center\">\r\n      Golden Trident Contest\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n\r\n<div class=\"golden-mobile\">\r\n  <img src=\"../../assets/images/Component 16 – 1.png\" alt=\"\">\r\n</div>\r\n\r\n<ion-content class=\"secondary \">\r\n\r\n\r\n  <h1 class=\"ion-text-left ion-padding title-re-m\">\r\n    Golden Trident Contest\r\n  </h1>\r\n  <ion-grid>\r\n    <ion-row class=\"flex-re\">\r\n      <ion-col size=\"12\" sizeLg=\"5\" class=\"ion-justify-item-center\">\r\n\r\n        <div class=\"ion-text-center bg-color scroll-d golden-input-web\" color=\"secondary\">\r\n\r\n\r\n          <div class=\"edit-input input-contest\" color=\"secondary\">\r\n            <h5>Enter your details</h5>\r\n\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Educational Institution <sup\r\n                  style=\"color: #fff;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-input color=\"light\" (keyup)=\"validate()\" [(ngModel)]=\"edu_institution\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Age Group <sup\r\n                  style=\"color: #fff;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-select [(ngModel)]=\"age_group\" value=\"brown\" okText=\"Okay\" cancelText=\"Dismiss\">\r\n                <ion-select-option value=\"Group 1\">Group1 -(classes 7 to 12)</ion-select-option>\r\n                <ion-select-option value=\"Group 2\">Group2 -(classes 12 to post graduation)</ion-select-option>\r\n                <ion-select-option value=\"Group 3\">Group3 -(above post graduation)</ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">City</ion-label>\r\n              <ion-input [(ngModel)]=\"city\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">State</ion-label>\r\n              <ion-input [(ngModel)]=\"state\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\" color=\"secondary\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Referal Code <sup\r\n                  style=\"color: #fff;font-size: 20px; top:0;\">*</sup></ion-label>\r\n              <ion-input [(ngModel)]=\"referral_code\" (keyup)=\"validate()\"></ion-input>\r\n            </ion-item>\r\n          </div>\r\n\r\n\r\n\r\n\r\n          <div class=\"check-box-golden ion-margin-top\">\r\n            <ion-checkbox class=\"ion-check\" (ionChange)=\"validate()\" style=\"width: 15px; height: 15px;\"\r\n              [(ngModel)]=\"terms\">\r\n            </ion-checkbox> <span>I Accept\r\n              <u style=\"text-underline-position: under;\"><a style=\"color: #fff;\" (click)=\" openMyTerms()\">Terms &\r\n                  Conditions, </a></u>\r\n              <u style=\"text-underline-position: under;\"><a style=\"color: #fff;\" (click)=\" openMyPrivacy()\">Privacy\r\n                  Policy,</a></u>\r\n              <u style=\"text-underline-position: under;\"><a style=\"color: #fff;\" (click)=\" openMyRefund()\"> Refund &\r\n                  Cancellation Policy</a></u> by\r\n              aBull.</span>\r\n          </div>\r\n          <div *ngIf=\"errorMsg\" style=\"color: red;\">{{errorMsg}}</div>\r\n          <div class=\"ion-text-center ion-margin\" style=\"margin-bottom: 40px;\">\r\n            <div class=\"ion-text-center ion-margin\">\r\n\r\n              <ion-button class=\"btn-style\" color=\"warning\" [disabled]=\"!enableEnroll\" (click)=\"loadCheckout()\">Enroll\r\n                Now</ion-button>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </ion-col>\r\n      <ion-col size=\"12\" sizeLg=\"7\">\r\n        <div class=\"g-center-web\">\r\n          <div class=\"golden-webimg\">\r\n            <img src=\"../../assets/images/Component 16 – 1@3x.png\" alt=\"\">\r\n          </div>\r\n          <div class=\"flex-web\">\r\n\r\n            <div class=\"img-card\">\r\n\r\n              <h1 class=\"ion-text-center t-mobile\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title1}}</h1>\r\n\r\n              <img src=\"{{contestDetails?.url1}}\" alt=\"\">\r\n              <div>\r\n                <h1 class=\" t-web\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title1}}</h1>\r\n                <!-- <ion-title class=\"t-web\"> Golden Trident Contest Section Title 1</ion-title> -->\r\n                <p>{{contestDetails?.description1}}</p>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"img-card\">\r\n              <h1 class=\"ion-text-center t-mobile\" style=\"font-size: 1.1rem;\">{{contestDetails?.title2}}</h1>\r\n\r\n              <img src=\"{{contestDetails?.url2}}\" alt=\"\">\r\n              <div>\r\n                <h1 class=\" t-web\" style=\"font-size: 1.1rem;\"> {{contestDetails?.title2}}</h1>\r\n                <!-- <ion-title class=\"t-web\"> Golden Trident Contest Section Title 2</ion-title> -->\r\n                <p>{{contestDetails?.description2}}</p>\r\n              </div>\r\n\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n\r\n\r\n\r\n\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_tab3_tab3_module_ts.js.map