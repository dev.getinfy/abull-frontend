(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_edit-profile_edit-profile_module_ts"],{

/***/ 1871:
/*!*************************************************************!*\
  !*** ./src/app/edit-profile/edit-profile-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EditProfilePageRoutingModule": () => (/* binding */ EditProfilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit-profile.page */ 2613);




const routes = [
    {
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_0__.EditProfilePage
    }
];
let EditProfilePageRoutingModule = class EditProfilePageRoutingModule {
};
EditProfilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], EditProfilePageRoutingModule);



/***/ }),

/***/ 1241:
/*!*****************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EditProfilePageModule": () => (/* binding */ EditProfilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit-profile-routing.module */ 1871);
/* harmony import */ var _edit_profile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-profile.page */ 2613);







let EditProfilePageModule = class EditProfilePageModule {
};
EditProfilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.EditProfilePageRoutingModule
        ],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_1__.EditProfilePage]
    })
], EditProfilePageModule);



/***/ }),

/***/ 2613:
/*!***************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EditProfilePage": () => (/* binding */ EditProfilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_edit_profile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./edit-profile.page.html */ 9439);
/* harmony import */ var _edit_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-profile.page.scss */ 5494);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../a-bull-api.service */ 3359);






let EditProfilePage = class EditProfilePage {
    constructor(router, aBullservice) {
        this.router = router;
        this.aBullservice = aBullservice;
        this.countries = ["India", "United States", "United Kingdom", "Australia", "United Arab Emirates", "China", "Canada", "Japan", "Germany"];
        this.user = JSON.parse(localStorage.getItem("loggedInUser"));
        console.log(this.user);
    }
    ngOnInit() {
    }
    onEditProfile() {
        let data = {
            "first_name": this.user.first_name,
            "last_name": this.user.last_name,
            "email": this.user.email,
            "phone_number": this.user.phone_number,
            "country": this.user.country,
            "dob": this.user.dob
        };
        this.aBullservice.aBullEditProfile(data).subscribe(res => {
            console.log(res, "...");
            localStorage.setItem('loggedInUser', JSON.stringify(data));
        });
        // console.log(this.user)
    }
};
EditProfilePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService }
];
EditProfilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-edit-profile',
        template: _raw_loader_edit_profile_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_edit_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], EditProfilePage);



/***/ }),

/***/ 5494:
/*!*****************************************************!*\
  !*** ./src/app/edit-profile/edit-profile.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LXByb2ZpbGUucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ 9439:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-profile/edit-profile.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-icon name=\"chevron-back-outline\" class=\"back-icon\" style=\"position: unset;\" routerLink=\"/app/tabs/profile\"></ion-icon>\r\n    <ion-title class=\"ion-text-center\">Edit-Profile</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"12\" sizeLg=\"12\" class=\"ion-align-self-center\">\r\n        <div class=\"card-profile\">\r\n          <div class=\"user\">\r\n            <img class=\"user-avatar\"\r\n              src=\"https://www.clipartmax.com/png/middle/171-1717870_stockvader-predicted-cron-for-may-user-profile-icon-png.png\">\r\n        \r\n            <img class=\"icon-edit\" src=\"../../assets/icon/Component 13 – 2.svg\" alt=\"\">\r\n            <div class=\"user-text\">\r\n              <ion-text>Edit Profile</ion-text>\r\n            </div>\r\n          </div>\r\n        \r\n          <div class=\"edit-input\">\r\n            <ion-item lines=\"none\" class=\"float-left\">\r\n              <ion-label  class=\"label-title\" position=\"stacked\">First Name <sup style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-input [(ngModel)]=\"user.first_name\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label  class=\"label-title\" position=\"stacked\">Last Name</ion-label>\r\n              <ion-input [(ngModel)]=\"user.last_name\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label  class=\"label-title\" position=\"stacked\">Phone Number <sup style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-input [(ngModel)]=\"user.phone_number\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label  class=\"label-title\" position=\"stacked\">Email Address <sup style=\"color: #ff0003;font-size: 20px; top: 0;\">*</sup></ion-label>\r\n              <ion-input [(ngModel)]=\"user.email\"></ion-input>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label class=\"label-title\" position=\"stacked\">Country</ion-label>\r\n              <ion-select okText=\"Okay\" cancelText=\"Dismiss\" [(ngModel)]=\"user.country\">\r\n                <ion-select-option *ngFor=\"let item of countries\" [value]=\"item\">{{item}} </ion-select-option>\r\n              </ion-select>\r\n            </ion-item>\r\n            <ion-item lines=\"none\">\r\n              <ion-label  class=\"label-title\" position=\"stacked\">Date Of Birth</ion-label>\r\n              <ion-input type=\"date\" [(ngModel)]=\"user.dob\"></ion-input>\r\n            </ion-item>\r\n            <div style=\"margin: 3rem 0;\">\r\n              <ion-button class=\"btn-style web-btn\" (click)=\"onEditProfile()\"  color=\"warning\">Update</ion-button>\r\n            </div>\r\n        \r\n          </div>\r\n        </div>\r\n       \r\n      </ion-col>\r\n      <!-- <ion-col size=\"12\" sizeLg=\"2\">\r\n\r\n      </ion-col> -->\r\n    </ion-row>\r\n  </ion-grid>\r\n \r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_edit-profile_edit-profile_module_ts.js.map