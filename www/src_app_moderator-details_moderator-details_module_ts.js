(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_moderator-details_moderator-details_module_ts"],{

/***/ 5927:
/*!***********************************************************************!*\
  !*** ./src/app/moderator-details/moderator-details-routing.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModeratorDetailsPageRoutingModule": () => (/* binding */ ModeratorDetailsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _moderator_details_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moderator-details.page */ 2482);




const routes = [
    {
        path: '',
        component: _moderator_details_page__WEBPACK_IMPORTED_MODULE_0__.ModeratorDetailsPage
    }
];
let ModeratorDetailsPageRoutingModule = class ModeratorDetailsPageRoutingModule {
};
ModeratorDetailsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ModeratorDetailsPageRoutingModule);



/***/ }),

/***/ 7096:
/*!***************************************************************!*\
  !*** ./src/app/moderator-details/moderator-details.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModeratorDetailsPageModule": () => (/* binding */ ModeratorDetailsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _moderator_details_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./moderator-details-routing.module */ 5927);
/* harmony import */ var _moderator_details_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moderator-details.page */ 2482);







let ModeratorDetailsPageModule = class ModeratorDetailsPageModule {
};
ModeratorDetailsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _moderator_details_routing_module__WEBPACK_IMPORTED_MODULE_0__.ModeratorDetailsPageRoutingModule
        ],
        declarations: [_moderator_details_page__WEBPACK_IMPORTED_MODULE_1__.ModeratorDetailsPage]
    })
], ModeratorDetailsPageModule);



/***/ }),

/***/ 2482:
/*!*************************************************************!*\
  !*** ./src/app/moderator-details/moderator-details.page.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModeratorDetailsPage": () => (/* binding */ ModeratorDetailsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_moderator_details_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./moderator-details.page.html */ 9548);
/* harmony import */ var _moderator_details_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./moderator-details.page.scss */ 4497);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);




let ModeratorDetailsPage = class ModeratorDetailsPage {
    constructor() { }
    ngOnInit() {
    }
};
ModeratorDetailsPage.ctorParameters = () => [];
ModeratorDetailsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-moderator-details',
        template: _raw_loader_moderator_details_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_moderator_details_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ModeratorDetailsPage);



/***/ }),

/***/ 4497:
/*!***************************************************************!*\
  !*** ./src/app/moderator-details/moderator-details.page.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtb2RlcmF0b3ItZGV0YWlscy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ 9548:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/moderator-details/moderator-details.page.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-icon name=\"chevron-back-outline\" class=\"back-icon\" style=\"position: unset;\"></ion-icon>\r\n    <ion-title class=\"ion-text-center\">\r\n      Moderator Details\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"12\" sizeLg=\"9\">\r\n\r\n      </ion-col>\r\n      <ion-col size=\"12\" sizeLg=\"9\">\r\n        <div class=\"content-center-w\">\r\n          <div class=\"moderator-page\">\r\n            <ion-grid>\r\n              <ion-row>\r\n                <ion-col size=\"12\" sizeLg=\"4\">\r\n                 \r\n                  <ion-item lines=\"none\">\r\n                    <div class=\"flex-row\">\r\n                      <ion-avatar slot=\"start\">\r\n                        <img src=\"../../assets/images/Component 19 – 1.png\">\r\n                      </ion-avatar>\r\n                      <ion-label>\r\n                        <h3>Moderator Name</h3>\r\n                        <p>23 Articles </p>\r\n                        <p>\r\n                          <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                          <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                          <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                          <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                          <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                        </p>\r\n                        <p class=\"social-icons\">\r\n                          <a href=\"#\"> <img src=\"../../assets/images/twitter-color.png\" alt=\"\"> </a>\r\n                          <a href=\"#\"> <img src=\"../../assets/images/instagram-color.png\" alt=\"\"> </a>\r\n                          <a href=\"#\"> <img src=\"../../assets/images/facebook-color.png\" alt=\"\"> </a>\r\n                          <a href=\"#\"> <img src=\"../../assets/images/linkedin-color.png\" alt=\"\"> </a>\r\n    \r\n                        </p>\r\n                      </ion-label>\r\n                    </div>\r\n                   \r\n                  </ion-item>\r\n                </ion-col>\r\n                <ion-col size=\"12\" sizeLg=\"8\" class=\"ion-align-self-left\">\r\n                  <div class=\"user-about\">\r\n                    <h1>About</h1>\r\n                    <p>\r\n                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.\r\n                      Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.\r\n                      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.\r\n                      Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.\r\n                    </p>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-grid>\r\n          </div>\r\n            <!-- Articles section -->\r\n            <ion-item lines=\"none\" class=\"art-section\">\r\n              <ion-label slot=\"start\"><b>Recent Articles</b></ion-label>\r\n              <ion-label slot=\"end\">Most relevant <img src=\"../../assets/icon/Component 12 – 2.svg\" alt=\"\"></ion-label>\r\n            </ion-item>\r\n  \r\n  \r\n            <div class=\"page-info home-content\">\r\n              <div class=\"text-content ion-padding-vertical\" style=\"margin-top: 10px;\">\r\n                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.</p>\r\n              </div>\r\n              <div class=\"banner-img\">\r\n                <img src=\"../../assets/images/mario-gogh-VBLHICVh-lI-unsplash.png\" alt=\"\">\r\n              </div>\r\n              <ion-footer class=\"ion-no-border ion-padding\">\r\n                <div class=\"bottom-align-item\">\r\n                  <ion-button color=\"primary\" fill=\"outline\">\r\n                    <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n                  </ion-button>\r\n                  <ion-button color=\"success\" fill=\"outline\">\r\n                    <img src=\"../../assets/icon/Component 18 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n                  </ion-button>\r\n                  <ion-button color=\"danger\" fill=\"outline\">\r\n                    <img src=\"../../assets/icon/Path 58.svg\" alt=\"\">&nbsp;<span>Share</span>\r\n                  </ion-button>\r\n                </div>\r\n              </ion-footer>\r\n            </div>\r\n        </div>\r\n      \r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n\r\n\r\n\r\n\r\n\r\n<!-- <ion-card>\r\n    <ion-card-content>\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n    </ion-card-content>\r\n    <img src=\"https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__480.jpg\" alt=\"\">\r\n    <ion-footer class=\"ion-no-border ion-padding\">\r\n      <div class=\"bottom-align-item\">\r\n        <ion-button color=\"primary\">\r\n          <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n        </ion-button>\r\n        <ion-button color=\"success\">\r\n          <img src=\"../../assets/icon/Component 18 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n        </ion-button>\r\n        <ion-button color=\"danger\">\r\n          <img src=\"../../assets/icon/Path 58.svg\"  alt=\"\">&nbsp;<span>Share</span>\r\n        </ion-button>\r\n      </div>\r\n    </ion-footer>\r\n  </ion-card> -->\r\n<!-- card-2 -->\r\n<!-- <ion-card>\r\n    <ion-card-content>\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n    </ion-card-content>\r\n    <ion-footer>\r\n      <ion-row>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"primary\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\"> &nbsp;<div>203 &nbsp;Likes </div>\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"success\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/Component 18 – 1.svg\" alt=\"\"> &nbsp; <div>02 &nbsp;comments </div>\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"danger\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/Path 58.svg\" alt=\"\"> &nbsp;<div>Share </div>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-footer>\r\n  </ion-card> -->\r\n<!-- card-3 -->\r\n<!-- <ion-card>\r\n    <ion-card-content>\r\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus iaculis mollis ligula sed ultrices.\r\n      Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.\r\n      Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.\r\n    </ion-card-content>\r\n    <ion-footer>\r\n      <ion-row>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"primary\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\"> &nbsp;<div>203 &nbsp;Likes </div>\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"success\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/Component 18 – 1.svg\" alt=\"\"> &nbsp; <div>02 &nbsp;comments </div>\r\n          </ion-button>\r\n        </ion-col>\r\n        <ion-col center text-center>\r\n          <ion-button color=\"danger\" fill=\"outline\">\r\n            <img src=\"../../assets/icon/Path 58.svg\" alt=\"\"> &nbsp;<div>Share </div>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-footer>\r\n  </ion-card> -->");

/***/ })

}]);
//# sourceMappingURL=src_app_moderator-details_moderator-details_module_ts.js.map