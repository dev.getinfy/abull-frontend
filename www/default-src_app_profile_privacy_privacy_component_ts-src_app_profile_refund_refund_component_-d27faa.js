(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["default-src_app_profile_privacy_privacy_component_ts-src_app_profile_refund_refund_component_-d27faa"],{

/***/ 8159:
/*!******************************************************!*\
  !*** ./src/app/profile/privacy/privacy.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PrivacyComponent": () => (/* binding */ PrivacyComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_privacy_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./privacy.component.html */ 8004);
/* harmony import */ var _privacy_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./privacy.component.scss */ 513);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 476);





let PrivacyComponent = class PrivacyComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    ngOnInit() { }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            dismissed: true
        });
    }
};
PrivacyComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ModalController }
];
PrivacyComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-privacy',
        template: _raw_loader_privacy_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_privacy_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PrivacyComponent);



/***/ }),

/***/ 6173:
/*!****************************************************!*\
  !*** ./src/app/profile/refund/refund.component.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RefundComponent": () => (/* binding */ RefundComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_refund_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./refund.component.html */ 8185);
/* harmony import */ var _refund_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./refund.component.scss */ 3489);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 476);





let RefundComponent = class RefundComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    ngOnInit() { }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            dismissed: true
        });
    }
};
RefundComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ModalController }
];
RefundComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-refund',
        template: _raw_loader_refund_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_refund_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RefundComponent);



/***/ }),

/***/ 6813:
/*!**************************************************!*\
  !*** ./src/app/profile/terms/terms.component.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TermsComponent": () => (/* binding */ TermsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_terms_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./terms.component.html */ 6576);
/* harmony import */ var _terms_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./terms.component.scss */ 1314);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 476);





let TermsComponent = class TermsComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    ngOnInit() { }
    dismiss() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modalController.dismiss({
            dismissed: true
        });
    }
};
TermsComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ModalController }
];
TermsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-terms',
        template: _raw_loader_terms_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_terms_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TermsComponent);



/***/ }),

/***/ 513:
/*!********************************************************!*\
  !*** ./src/app/profile/privacy/privacy.component.scss ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcml2YWN5LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ 3489:
/*!******************************************************!*\
  !*** ./src/app/profile/refund/refund.component.scss ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZWZ1bmQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 1314:
/*!****************************************************!*\
  !*** ./src/app/profile/terms/terms.component.scss ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZXJtcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 8004:
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/privacy/privacy.component.html ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\r\n\r\n  <ion-icon name=\"close-outline\" class=\"ion-float-right\" style=\"padding: .7rem; font-size: 1.4rem;\" (click)=\"dismiss()\">\r\n  </ion-icon>\r\n\r\n</ion-header>\r\n <ion-content class=\"ion-padding\">\r\n\r\n  <div>\r\n    <h1 style=\"text-align: center;\">Privacy Policy</h1>\r\n    <b>Your website may use the Privacy Policy given below:</b>\r\n    <p>The terms \"We\" / \"Us\" / \"Our\"/”Company” individually and collectively refer to Abull technologies Private Limited\r\n      and the terms \"You\" /\"Your\" / \"Yourself\" refer to the users.</p>\r\n    <p>This Privacy Policy is an electronic record in the form of an electronic contract formed under the information\r\n      Technology Act, 2000 and the rules made thereunder and the amended provisions pertaining to electronic documents /\r\n      records in various statutes as amended by the information Technology Act, 2000. This Privacy Policy does not\r\n      require any physical, electronic or digital signature.</p>\r\n    <p>This Privacy Policy is a legally binding document between you and Abull technologies Private Limited (both terms\r\n      defined below). The terms of this Privacy Policy will be effective upon your acceptance of the same (directly or\r\n      indirectly in electronic form, by clicking on the I accept tab or by use of the website or by other means) and\r\n      will govern the relationship between you and Abull technologies Private Limited for your use of the website\r\n      “<b>abull.app</b>” (defined below).</p>\r\n    <p>This document is published and shall be construed in accordance with the provisions of the Information Technology\r\n      (reasonable security practices and procedures and sensitive personal data of information) rules, 2011 under\r\n      Information Technology Act, 2000; that require publishing of the Privacy Policy for collection, use, storage and\r\n      transfer of sensitive personal data or information.</p>\r\n    <p>Please read this Privacy Policy carefully by using the Website, you indicate that you understand, agree and\r\n      consent to this Privacy Policy. If you do not agree with the terms of this Privacy Policy, please do not use this\r\n      Website.</p>\r\n    <p> By providing us your Information or by making use of the facilities provided by the Website, You hereby consent\r\n      to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal\r\n      Information by us as specified under this Privacy Policy. You further agree that such collection, use, storage and\r\n      transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.</p>\r\n    <h3>USER INFORMATION </h3>\r\n    <p> To avail certain services on our Websites, users are required to provide certain information for the\r\n      registration process namely: - a) your name, b) email address, c) sex, d) age, e) PIN code, f) credit card or\r\n      debit card details g) password etc., and / or your occupation, interests, and the like. The Information as\r\n      supplied by the users enables us to improve our sites and provide you the most user-friendly experience.</p>\r\n\r\n    <p>All required information is service dependent and we may use the above said user information to, maintain,\r\n      protect, and improve its services (including advertising services) and for developing new services</p>\r\n\r\n    <p>Such information will not be considered as sensitive if it is freely available and accessible in the public\r\n      domain or is furnished under the Right to Information Act, 2005 or any other law for the time being in force.</p>\r\n\r\n    <h3>COOKIES</h3>\r\n    <p>improve the responsiveness of the sites for our users, we may use \"cookies\", or similar electronic tools to\r\n      collect information to assign each visitor a unique, random number as a User Identification (User ID) to\r\n      understand the user's individual interests using the Identified Computer. Unless you voluntarily identify yourself\r\n      (through registration, for example), we will have no way of knowing who you are, even if we assign a cookie to\r\n      your computer. The only personal information a cookie can contain is information you supply (an example of this is\r\n      when you ask for our Personalised Horoscope). A cookie cannot read data off your hard drive. Our advertisers may\r\n      also assign their own cookies to your browser (if you click on their ads), a process that we do not control.</p>\r\n\r\n    <p> web servers automatically collect limited information about your computer's connection to the Internet,\r\n      including your IP address, when you visit our site. (Your IP address is a number that lets computers attached to\r\n      the Internet know where to send you data -- such as the web pages you view.) Your IP address does not identify you\r\n      personally. We use this information to deliver our web pages to you upon request, to tailor our site to the\r\n      interests of our users, to measure traffic within our site and let advertisers know the geographic locations from\r\n      where our visitors come.</p>\r\n    <h3>LINKS TO THE OTHER SITES</h3>\r\n    <p> Our policy discloses the privacy practices for our own web site only. Our site provides links to other websites\r\n      also that are beyond our control. We shall in no way be responsible in way for your use of such sites.5.</p>\r\n    <h3>INFORMATION SHARING</h3>\r\n    <p> We shares the sensitive personal information to any third party without obtaining the prior consent of the user\r\n      in the following limited circumstances:</p>\r\n\r\n    <p><b>(a)</b>When it is requested or required by law or by any court or governmental agency or authority to\r\n      disclose, for the purpose of verification of identity, or for the prevention, detection, investigation including\r\n      cyber incidents, or for prosecution and punishment of offences. These disclosures are made in good faith and\r\n      belief that such disclosure is reasonably necessary for enforcing these Terms; for complying with the applicable\r\n      laws and regulations.</p>\r\n\r\n    <p><b>(b)</b> We proposes to share such information within its group companies and officers and employees of such\r\n      group companies for the purpose of processing personal information on its behalf. We also ensure that these\r\n      recipients of such information agree to process such information based on our instructions and in compliance with\r\n      this Privacy Policy and any other appropriate confidentiality and security measures.</p>\r\n\r\n    <h3>INFORMATION SECURITY</h3>\r\n    <p> take appropriate security measures to protect against unauthorized access to or unauthorized alteration,\r\n      disclosure or destruction of data. These include internal reviews of our data collection, storage and processing\r\n      practices and security measures, including appropriate encryption and physical security measures to guard against\r\n      unauthorized access to systems where we store personal data.</p>\r\n\r\n    <p> information gathered on our Website is securely stored within our controlled database. The database is stored on\r\n      servers secured behind a firewall; access to the servers is password-protected and is strictly limited. However,\r\n      as effective as our security measures are, no security system is impenetrable. We cannot guarantee the security of\r\n      our database, nor can we guarantee that information you supply will not be intercepted while being transmitted to\r\n      us over the Internet. And, of course, any information you include in a posting to the discussion areas is\r\n      available to anyone with Internet access.</p>\r\n\r\n    <p>However the internet is an ever evolving medium. We may change our Privacy Policy from time to time to\r\n      incorporate necessary future changes. Of course, our use of any information we gather will always be consistent\r\n      with the policy under which the information was collected, regardless of what the new policy may be.</p>\r\n\r\n    <h3>Grievance Redressal</h3>\r\n    <p> Mechanism: Any complaints, abuse or concerns with regards to content and or comment or breach of these terms\r\n      shall be immediately informed to the designated Grievance Officer as mentioned below via in writing or through\r\n      email signed with the electronic signature to tech head (\"Grievance Officer\").</p>\r\n    <div>\r\n      Mr. Srilalithaadithya.G (Grievance Officer)\r\n      reach@abull.io\r\n      Company Name & Address\r\n      Email: reach@abull.io\r\n      Ph: +91 73822 44945\r\n      Address:\r\n      C34, BN Reddy Towers, Avanthi Nagar, Basheerbagh, Hyderabad 500029\r\n      Operations: T-Hub, CIE, IIIT-H ,Old Bombay road, Gachibowli 500019.\r\n    </div>\r\n\r\n  </div>\r\n \r\n </ion-content>");

/***/ }),

/***/ 8185:
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/refund/refund.component.html ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\r\n\r\n  <ion-icon name=\"close-outline\" class=\"ion-float-right\" style=\"padding: .7rem; font-size: 1.4rem;\" (click)=\"dismiss()\">\r\n  </ion-icon>\r\n\r\n</ion-header>\r\n <ion-content class=\"ion-padding\">\r\n\r\n\r\n  <div>\r\n    <h1 style=\"text-align: center;\"> Refund and Cancellation Policy</h1>\r\n    <p>focus is complete customer satisfaction. In the event, if you are payment has failed due to unforeseen\r\n    digital glitch or any service scope related issues such as rescheduled event or course or any issue in which our\r\n    functionality has caused in convenience and unable to deliver even after request we will refund back the money,\r\n    provided the reasons are genuine and proved after investigation. Please read the fine prints of each deal before\r\n    buying it, it provides all the details about the services or the product you purchase.</p>\r\n\r\n\r\n    <h3>Cancellation Policy</h3>\r\n\r\n    <p>For Cancellations please contact us via contact us link reach@abull.io. </p>\r\n    <p> Requests received within 30 days (post initial payment) are allowed for cancellation of services.</p>\r\n\r\n    <h3>Refund Policy</h3>\r\n\r\n    <p>We will try our best to create the suitable design concepts for our clients.</p>\r\n\r\n\r\n    <p>If paid by credit card, refunds will be issued to the original credit card provided at the time of purchase and\r\n      in case of payment gateway name payments refund will be made to the same account.\r\n    </p>\r\n  </div>\r\n </ion-content>");

/***/ }),

/***/ 6576:
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/terms/terms.component.html ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\r\n\r\n  <ion-icon name=\"close-outline\" class=\"ion-float-right\" style=\"padding: .7rem; font-size: 1.4rem;\" (click)=\"dismiss()\">\r\n  </ion-icon>\r\n\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n  <h1 style=\"text-align: center;\"> TERMS AND CONDITIONS</h1>\r\n  <b>Your website may use the Terms and Conditions given below.</b>\r\n  <p>The terms \"We\" / \"Us\" / \"Our\"/”Company” individually and collectively refer to ……………aBull technologies Pvt\r\n    Ltd…………..and the terms \"Visitor” ”User” refer to the users. </p>\r\n\r\n  <p>This page states the Terms and Conditions under which you (Visitor) may visit this website (“Website”). Please read\r\n    this page carefully. If you do not accept the Terms and Conditions stated here, we would request you to exit this\r\n    site. The business, any of its business divisions and / or its subsidiaries, associate companies or subsidiaries to\r\n    subsidiaries or such other investment companies (in India or abroad) reserve their respective rights to revise these\r\n    Terms and Conditions at any time by updating this posting. You should visit this page periodically to re-appraise\r\n    yourself of the Terms and Conditions, because they are binding on all users of this Website.</p>\r\n\r\n  <h3>USE OF CONTENT</h3>\r\n\r\n  <p>All logos, brands, marks headings, labels, names, signatures, numerals, shapes or any combinations thereof,\r\n    appearing in this site, except as otherwise noted, are properties either owned, or used under licence, by the\r\n    business and / or its associate entities who feature on this Website. The use of these properties or any other\r\n    content on this site, except as provided in these terms and conditions or in the site content, is strictly\r\n    prohibited.</p>\r\n\r\n  <p>You may not sell or modify the content of this Website or reproduce, display, publicly perform, distribute, or\r\n    otherwise use the materials in any way for any public or commercial purpose without the respective organisation’s or\r\n    entity’s written permission.</p>\r\n\r\n  <h3>ACCEPTABLE WEBSITE USE</h3>\r\n\r\n  <h3>(A) Security Rules</h3>\r\n  <p>Visitors are prohibited from violating or attempting to violate the security of the Web site, including, without\r\n    limitation, (1) accessing data not intended for such user or logging into a server or account which the user is not\r\n    authorised to access, (2) attempting to probe, scan or test the vulnerability of a system or network or to breach\r\n    security or authentication measures without proper authorisation, (3) attempting to interfere with service to any\r\n    user, host or network, including, without limitation, via means of submitting a virus or \"Trojan horse\" to the\r\n    Website, overloading, \"flooding\", \"mail bombing\" or \"crashing\", or (4) sending unsolicited electronic mail,\r\n    including promotions and/or advertising of products or services. Violations of system or network security may result\r\n    in civil or criminal liability. The business and / or its associate entities will have the right to investigate\r\n    occurrences that they suspect as involving such violations and will have the right to involve, and cooperate with,\r\n    law enforcement authorities in prosecuting users who are involved in such violations.</p>\r\n\r\n  <h3>(B) General Rules</h3>\r\n  <p>Visitors may not use the Web Site in order to transmit, distribute, store or destroy material (a) that could\r\n    constitute or encourage conduct that would be considered a criminal offence or violate any applicable law or\r\n    regulation, (b) in a manner that will infringe the copyright, trademark, trade secret or other intellectual property\r\n    rights of others or violate the privacy or publicity of other personal rights of others, or (c) that is libellous,\r\n    defamatory, pornographic, profane, obscene, threatening, abusive or hateful.</p>\r\n\r\n  <h3>INDEMNITY</h3>\r\n\r\n  <p>The User unilaterally agree to indemnify and hold harmless, without objection, the Company, its officers,\r\n    directors, employees and agents from and against any claims, actions and/or demands and/or liabilities and/or losses\r\n    and/or damages whatsoever arising from or resulting from their use of www.abull.io / www. abull.app or their breach\r\n    of the terms . </p>\r\n\r\n  <p>Legal action: Any dispute concerning service or otherwise pertaining to the website/application shall be reported\r\n    with sessions judge Hyderabad- Secunderabad, Telangana, India jurisdiction only.</p>\r\n\r\n  <h3>LIABILITY </h3>\r\n\r\n  <p>User agrees that neither Company nor its group companies, directors, officers or employee shall be liable for any\r\n    direct or/and indirect or/and incidental or/and special or/and consequential or/and exemplary damages, resulting\r\n    from the use or/and the inability to use the service or/and for cost of procurement of substitute goods or/and\r\n    services or resulting from any goods or/and data or/and information or/and services purchased or/and obtained or/and\r\n    messages received or/and transactions entered into through or/and from the service or/and resulting from\r\n    unauthorized access to or/and alteration of user's transmissions or/and data or/and arising from any other matter\r\n    relating to the service, including but not limited to, damages for loss of profits or/and use or/and data or other\r\n    intangible, even if Company has been advised of the possibility of such damages.\r\n    User further agrees that Company shall not be liable for any damages arising from interruption, suspension or\r\n    termination of service, including but not limited to direct or/and indirect or/and incidental or/and special\r\n    consequential or/and exemplary damages, whether such interruption or/and suspension or/and termination was justified\r\n    or not, negligent or intentional, inadvertent or advertent.\r\n    User agrees that Company shall not be responsible or liable to user, or anyone, for the statements or conduct of any\r\n    third party of the service. In sum, in no event shall Company's total liability to the User for all damages or/and\r\n    losses or/and causes of action exceed the amount paid by the User to Company, if any, that is related to the cause\r\n    of action.</p>\r\n\r\n  <h3>DISCLAIMER OF CONSEQUENTIAL DAMAGES</h3>\r\n\r\n  <p>In no event shall Company or any parties, organizations or entities associated with the corporate brand name us or\r\n    otherwise, mentioned at this Website be liable for any damages whatsoever (including, without limitations,\r\n    incidental and consequential damages, lost profits, or damage to computer hardware or loss of data information or\r\n    business interruption) resulting from the use or inability to use the Website and the Website material, whether\r\n    based on warranty, contract, tort, or any other legal theory, and whether or not, such organization or entities were\r\n    advised of the possibility of such damages.</p>\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=default-src_app_profile_privacy_privacy_component_ts-src_app_profile_refund_refund_component_-d27faa.js.map