(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_paysuccess_paysuccess_module_ts"],{

/***/ 7979:
/*!*********************************************************!*\
  !*** ./src/app/paysuccess/paysuccess-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaysuccessPageRoutingModule": () => (/* binding */ PaysuccessPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _paysuccess_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./paysuccess.page */ 7855);




const routes = [
    {
        path: '',
        component: _paysuccess_page__WEBPACK_IMPORTED_MODULE_0__.PaysuccessPage
    }
];
let PaysuccessPageRoutingModule = class PaysuccessPageRoutingModule {
};
PaysuccessPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PaysuccessPageRoutingModule);



/***/ }),

/***/ 9094:
/*!*************************************************!*\
  !*** ./src/app/paysuccess/paysuccess.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaysuccessPageModule": () => (/* binding */ PaysuccessPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _paysuccess_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./paysuccess-routing.module */ 7979);
/* harmony import */ var _paysuccess_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./paysuccess.page */ 7855);







let PaysuccessPageModule = class PaysuccessPageModule {
};
PaysuccessPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _paysuccess_routing_module__WEBPACK_IMPORTED_MODULE_0__.PaysuccessPageRoutingModule
        ],
        declarations: [_paysuccess_page__WEBPACK_IMPORTED_MODULE_1__.PaysuccessPage]
    })
], PaysuccessPageModule);



/***/ }),

/***/ 7855:
/*!***********************************************!*\
  !*** ./src/app/paysuccess/paysuccess.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PaysuccessPage": () => (/* binding */ PaysuccessPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_paysuccess_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./paysuccess.page.html */ 370);
/* harmony import */ var _paysuccess_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./paysuccess.page.scss */ 6035);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../a-bull-api.service */ 3359);






let PaysuccessPage = class PaysuccessPage {
    constructor(aBull, route, aBUllservice) {
        this.aBull = aBull;
        this.route = route;
        this.aBUllservice = aBUllservice;
    }
    ngOnInit() {
        //this.aBUllGTCRegistration();
        this.getaBUllGTCDetails();
    }
    aBUllGTCRegistration() {
        try {
            this.details = JSON.parse(localStorage.getItem('gtsdetails'));
            this.route.queryParamMap.subscribe((params) => {
                console.log(this.details);
                if (this.details) {
                    if (params['params']['payment_id']) {
                        this.details["transaction_details"]["trans_id"] = params['params']['payment_id'];
                    }
                    this.aBull.aBUllGTCRegistration(this.details).subscribe(res => {
                        console.log(res);
                    });
                }
            });
        }
        catch (error) {
        }
    }
    getaBUllGTCDetails() {
        this.aBUllservice.aBullContestDetails().subscribe(res => {
            if (res && res.data) {
                this.contestDetails = res.data[0];
            }
            console.log(res);
        });
    }
};
PaysuccessPage.ctorParameters = () => [
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute },
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService }
];
PaysuccessPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-paysuccess',
        template: _raw_loader_paysuccess_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_paysuccess_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PaysuccessPage);



/***/ }),

/***/ 6035:
/*!*************************************************!*\
  !*** ./src/app/paysuccess/paysuccess.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYXlzdWNjZXNzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 370:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/paysuccess/paysuccess.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"header-moblie\">\r\n  <ion-toolbar>\r\n    <!-- <ion-icon name=\"chevron-back-outline\" class=\"back-icon\" style=\"position: unset;\"></ion-icon> -->\r\n    <ion-title class=\"ion-text-center\">\r\n      Golden Trident Contest\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<div class=\"golden-mobile\">\r\n  <img src=\"../../assets/images/Component 16 – 1.png\" alt=\"\">\r\n</div>\r\n<ion-content>\r\n\r\n  <ion-title class=\"ion-text-left ion-padding title-re-m\" style=\"margin-left: 90px; padding: 20px; font-weight: bold;\">\r\n    Golden Trident Contest\r\n  </ion-title>\r\n<ion-grid>\r\n  <ion-row >\r\n    <ion-col size=\"12\" sizeLg=\"9\">\r\n\r\n\r\n      <div class=\"golden-webimg\">\r\n        <img src=\"../../assets/images/Component 16 – 1@3x.png\" alt=\"\">\r\n      </div>\r\n\r\n\r\n      <div class=\"ion-text-center\">\r\n        <ion-item-divider>\r\n          <div class=\"img-center img-1\" style=\"gap: 12px;\">\r\n            <img src=\"../../assets/icon/Checklistsuccess.svg\" alt=\"\">\r\n            <ion-text>You are successfully enrolled</ion-text>\r\n          </div>\r\n        </ion-item-divider>\r\n\r\n\r\n\r\n        <ion-item-divider *ngIf=\"contestDetails\">\r\n          <div class=\"img-center img-2 \">\r\n            <ion-icon name=\"alarm-outline\" color=\"primary\"></ion-icon>\r\n            <div>\r\n              <ion-text color=\"primary\">Event Start Date:</ion-text>\r\n            </div>\r\n            <ion-text color=\"primary\"><b>{{contestDetails.start_date | date:'medium'}}</b></ion-text>\r\n          </div>\r\n        </ion-item-divider>\r\n\r\n        <ion-item-divider *ngIf=\"contestDetails\">\r\n          <div class=\"img-center img-3\">\r\n            <ion-icon name=\"alarm-outline\" color=\"danger\"></ion-icon>\r\n            <div>\r\n              <ion-text color=\"danger\">Event End Date:</ion-text>\r\n            </div>\r\n            <ion-text color=\"danger\"><b>{{contestDetails.end_date | date:'medium'}}</b></ion-text>\r\n          </div>\r\n        </ion-item-divider>\r\n\r\n        <ion-item-divider  *ngIf=\"contestDetails\">\r\n          <div class=\"img-center img-4\">\r\n            <ion-icon name=\"alarm-outline\" color=\"success\"></ion-icon>\r\n            <div>\r\n              <ion-text color=\"success\"> Result Date:</ion-text>\r\n            </div>\r\n            <ion-text color=\"success\"><b>{{contestDetails.result_date | date:'medium'}}</b></ion-text>\r\n          </div>\r\n        </ion-item-divider>\r\n\r\n        <div class=\"term-condition\">\r\n          <p class=\"ion-margin bottom-line\"> Terms And Conditions</p>\r\n\r\n          <div class=\"ion-padding ion-text-left\" *ngIf=\"contestDetails?.terms_conditions\">\r\n\r\n            <div [innerHtml]=\"contestDetails.terms_conditions\">\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"12\" sizeLg=\"3\">\r\n\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-grid>\r\n\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_paysuccess_paysuccess_module_ts.js.map