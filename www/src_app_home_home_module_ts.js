(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_home_home_module_ts"],{

/***/ 2003:
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageRoutingModule": () => (/* binding */ HomePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 2267);




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], HomePageRoutingModule);



/***/ }),

/***/ 3467:
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": () => (/* binding */ HomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home-routing.module */ 2003);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page */ 2267);







let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_0__.HomePageRoutingModule
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_1__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 2267:
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": () => (/* binding */ HomePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./home.page.html */ 9764);
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.scss */ 2610);
/* harmony import */ var _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../a-bull-api.service */ 3359);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);






let HomePage = class HomePage {
    constructor(aBull, router) {
        this.aBull = aBull;
        this.router = router;
        this.postsList = [];
        this.aBullPostLists();
    }
    ngOnInit() {
    }
    aBullPostLists() {
        //   var responseEx =  {
        //     "status": "success",
        //     "data": [
        //         {
        //             "id": 1,
        //             "title": "First post",
        //             "description": "<p>Post Description</p>",
        //             "post_type": "Image",
        //             "file": "http://localhost/abull_admin/public/uploads/posts/8fXP64N6r2.jpg",
        //             "category_id": 1,
        //             "moderator_id": 2,
        //             "status": 1,
        //             "createdAt": "2021-10-26T01:25:57.000Z",
        //             "updatedAt": "2021-10-26T01:25:57.000Z",
        //             "deletedAt": null,
        //             "categoryId": 1,
        //             "ModeratorId": 2,
        //             "category": {
        //                 "id": 1,
        //                 "name": "Cat1",
        //                 "status": 1,
        //                 "sortorder": 1,
        //                 "createdAt": "2021-10-26T01:24:58.000Z",
        //                 "updatedAt": "2021-10-26T01:24:58.000Z",
        //                 "deletedAt": null
        //             },
        //             "Moderator": {
        //                 "id": 2,
        //                 "first_name": "Moderator",
        //                 "last_name": "1"
        //             }
        //         },
        //         {
        //             "id": 2,
        //             "title": "Post 2",
        //             "description": "<p>Post 2 Description</p>",
        //             "post_type": "Image",
        //             "file": "http://localhost/abull_admin/public/uploads/posts/xXof9axijF.jpg",
        //             "category_id": 2,
        //             "moderator_id": 2,
        //             "status": 1,
        //             "createdAt": "2021-10-26T01:26:28.000Z",
        //             "updatedAt": "2021-10-26T01:26:28.000Z",
        //             "deletedAt": null,
        //             "categoryId": 2,
        //             "ModeratorId": 2,
        //             "category": {
        //                 "id": 2,
        //                 "name": "cat2",
        //                 "status": 1,
        //                 "sortorder": 2,
        //                 "createdAt": "2021-10-26T02:35:47.000Z",
        //                 "updatedAt": "2021-10-26T02:35:47.000Z",
        //                 "deletedAt": null
        //             },
        //             "Moderator": {
        //                 "id": 2,
        //                 "first_name": "Moderator",
        //                 "last_name": "1"
        //             }
        //         }
        //     ],
        //     "message": "Posts List",
        //     "current_time": "2021-10-29T07:34:56.496Z"
        // };
        this.aBull.aBullPostLists().subscribe(res => {
            if (res.data) {
                this.postsList = res.data;
            }
            console.log(this.postsList);
        });
    }
    onPostClick(post) {
        this.selctedPost = post;
        // console.log(this.selctedPost.id);
        this.router.navigate(['post-details/' + post.id]);
    }
};
HomePage.ctorParameters = () => [
    { type: _a_bull_api_service__WEBPACK_IMPORTED_MODULE_2__.ABullApiService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
HomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HomePage);



/***/ }),

/***/ 2610:
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".img-logo {\n  width: 103px;\n  height: 33px;\n}\n\n.bell {\n  width: 20.85px;\n  height: 24px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUFDSjs7QUFDQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0FBRUoiLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW1nLWxvZ297XHJcbiAgICB3aWR0aDoxMDNweDtcclxuICAgIGhlaWdodDogMzNweDtcclxufVxyXG4uYmVsbHtcclxuICAgIHdpZHRoOjIwLjg1cHg7XHJcbiAgICBoZWlnaHQ6IDI0cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ 9764:
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar>\r\n    <div class=\"ion-float-left \">\r\n      <img class=\"header-logo\" src=\"../../assets/images/aBull Logo new1.png\" alt=\"abull logo\" />\r\n    </div>\r\n    <div class=\"ion-float-right\">\r\n      <img class=\"bell-icon\" src=\"../../assets/icon/Component 11 – 2.svg\" alt=\"bell icon\" />\r\n    </div>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col size=\"12\" sizeLg=\"9\"  class=\"ion-align-self-center\" *ngIf=\"!postsList || !postsList.length\">\r\n        <div class=\"content-center-w\">\r\n          <h1> No Posts Available.</h1>\r\n        </div>\r\n        </ion-col>\r\n      <ion-col size=\"12\" sizeLg=\"9\"  class=\"ion-align-self-center\">\r\n        <div class=\"content-center-w\">\r\n\r\n\r\n          <ng-container *ngFor=\"let post of postsList\">\r\n\r\n            <ion-item lines=\"none\" style=\"margin-top: 15px;\">\r\n              <ion-avatar slot=\"start\">\r\n                <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n              </ion-avatar>\r\n              <ion-label style=\"padding: 0 0.5rem;\">\r\n                <h3>{{post?.Moderator?.first_name}} {{post?.Moderator?.last_name}}</h3>\r\n                <p><img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                  <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                </p>\r\n              </ion-label>\r\n              <div class=\"ion-text-right\" style=\"color: #777777; font-size: .8rem;\">\r\n                <ul (click) = \"onPostClick(post)\">\r\n                  <li>{{post.createdAt | date:\"mediumDate\"}}</li>\r\n                </ul>\r\n\r\n              </div>\r\n\r\n            </ion-item>\r\n            <div class=\"page-info home-content\">\r\n              <div class=\"ion-padding text-content hand\" (click) = \"onPostClick(post)\">\r\n                <h1 >{{post.title}}</h1>\r\n                <div [innerHtml]=\"post.description\">\r\n\r\n                </div>\r\n\r\n              </div>\r\n\r\n\r\n              <div class=\"banner-img hand\" *ngIf=\"post?.file\" (click) = \"onPostClick(post)\">\r\n                <img src=\"{{post.file}}\" alt=\"\">\r\n              </div>\r\n\r\n\r\n              <!-- <ion-footer class=\"ion-no-border ion-padding\">\r\n                <div class=\"bottom-align-item\">\r\n                  <ion-button color=\"primary\">\r\n                    <img src=\"../../assets/icon/ic_thumb_up_24px-1.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n                  </ion-button>\r\n\r\n\r\n                  <ion-button color=\"success\">\r\n                    <img src=\"../../assets/icon/Component 17 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n                  </ion-button>\r\n\r\n                  <ion-button color=\"danger\">\r\n                    <img src=\"../../assets/icon/Path 58-1.svg\" alt=\"\">&nbsp;<span>Share</span>\r\n                  </ion-button>\r\n                </div>\r\n\r\n\r\n\r\n\r\n              </ion-footer> -->\r\n            </div>\r\n\r\n\r\n          </ng-container>\r\n\r\n  <!-- static -1 -->\r\n\r\n          <!-- <ion-item lines=\"none\" style=\"margin-top: 15px;\">\r\n            <ion-avatar slot=\"start\">\r\n              <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n            </ion-avatar>\r\n            <ion-label style=\"padding: 0 0.5rem;\">\r\n              <h3>Moderator 1</h3>\r\n              <p><img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n              </p>\r\n            </ion-label>\r\n            <div class=\"ion-text-right\" style=\"color: #777777; font-size: .8rem;\">\r\n              <ul>\r\n                <li>3 mins ago</li>\r\n              </ul>\r\n\r\n            </div>\r\n\r\n          </ion-item>\r\n          <div class=\"page-info home-content\">\r\n            <div class=\"ion-padding text-content\">\r\n              <h1>lorem ipsum dolor sit amet,</h1>\r\n              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae perferendis dignissimos autem\r\n                necessitatibus\r\n                porro\r\n                alias optio enim voluptatem facere ea cumque reprehenderit doloremque quasi modi,</p>\r\n            </div>\r\n\r\n\r\n            <div class=\"banner-img\">\r\n              <img src=\"https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__480.jpg\" alt=\"\">\r\n            </div>\r\n\r\n\r\n            <ion-footer class=\"ion-no-border ion-padding\">\r\n              <div class=\"bottom-align-item\">\r\n                <ion-button color=\"primary\">\r\n                  <img src=\"../../assets/icon/ic_thumb_up_24px-1.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n                </ion-button>\r\n\r\n\r\n                <ion-button color=\"success\">\r\n                  <img src=\"../../assets/icon/Component 17 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n                </ion-button>\r\n\r\n                <ion-button color=\"danger\">\r\n                  <img src=\"../../assets/icon/Path 58-1.svg\" alt=\"\">&nbsp;<span>Share</span>\r\n                </ion-button>\r\n              </div>\r\n\r\n\r\n\r\n\r\n            </ion-footer>\r\n          </div> -->\r\n\r\n          <!-- static-2 -->\r\n          <!-- <ion-item lines=\"none\" style=\"margin-top: 15px;\">\r\n            <ion-avatar slot=\"start\">\r\n              <img src=\"https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y\">\r\n            </ion-avatar>\r\n            <ion-label style=\"padding: 0 0.5rem;\">\r\n              <h3>Moderator 1</h3>\r\n              <p><img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n                <img class=\"trading-icon\" src=\"/assets/icon/Trading-3.svg\" alt=\"trading\">\r\n              </p>\r\n            </ion-label>\r\n            <div class=\"ion-text-right\" style=\"color: #777777; font-size: .8rem;\">\r\n              <ul>\r\n                <li>3 mins ago</li>\r\n              </ul>\r\n\r\n            </div>\r\n\r\n          </ion-item>\r\n          <div class=\"page-info home-content\">\r\n            <div class=\"ion-padding text-content\">\r\n              <h1>lorem ipsum dolor sit amet,</h1>\r\n              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae perferendis dignissimos autem\r\n                necessitatibus\r\n                porro\r\n                alias optio enim voluptatem facere ea cumque reprehenderit doloremque quasi modi, veritatis ratione,\r\n                labore\r\n                reiciendis quis amet voluptatum odit iusto ipsam? Qui earum dolorum dolorem. Nostrum beatae esse in\r\n                nesciunt\r\n                facere consectetur laboriosam error debitis enim?</p>\r\n            </div>\r\n\r\n\r\n            <div class=\"banner-img\">\r\n              <img src=\"https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378__480.jpg\" alt=\"\">\r\n            </div>\r\n\r\n\r\n            <ion-footer class=\"ion-no-border ion-padding\">\r\n              <div class=\"bottom-align-item\">\r\n                <ion-button color=\"primary\" fill=\"outline\">\r\n                  <img src=\"../../assets/icon/ic_thumb_up_24px.svg\" alt=\"\">&nbsp;<span>203&nbsp;Likes</span>\r\n                </ion-button>\r\n\r\n\r\n                <ion-button color=\"success\" fill=\"outline\">\r\n                  <img src=\"../../assets/icon/Component 18 – 1.svg\" alt=\"\">&nbsp;<span>02&nbsp;comments</span>\r\n                </ion-button>\r\n\r\n                <ion-button color=\"danger\" fill=\"outline\">\r\n                  <img src=\"../../assets/icon/Path 58.svg\" alt=\"\">&nbsp;<span>Share</span>\r\n                </ion-button>\r\n              </div>\r\n\r\n\r\n\r\n\r\n            </ion-footer>\r\n          </div> -->\r\n\r\n        </div>\r\n      </ion-col>\r\n      <ion-col class=\"12\" sizeLg=\"3\">\r\n\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_home_home_module_ts.js.map