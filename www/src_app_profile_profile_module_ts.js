(self["webpackChunkaBull"] = self["webpackChunkaBull"] || []).push([["src_app_profile_profile_module_ts"],{

/***/ 6829:
/*!***************************************************!*\
  !*** ./src/app/profile/profile-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageRoutingModule": () => (/* binding */ ProfilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.page */ 2919);




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_0__.ProfilePage
    },
    {
        path: 'edit',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("common"), __webpack_require__.e("src_app_edit-profile_edit-profile_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ../edit-profile/edit-profile.module */ 1241)).then(m => m.EditProfilePageModule)
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ 4523:
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageModule": () => (/* binding */ ProfilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile-routing.module */ 6829);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page */ 2919);







let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProfilePageRoutingModule
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_1__.ProfilePage]
    })
], ProfilePageModule);



/***/ }),

/***/ 2919:
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePage": () => (/* binding */ ProfilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./profile.page.html */ 2907);
/* harmony import */ var _profile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page.scss */ 231);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _terms_terms_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./terms/terms.component */ 6813);
/* harmony import */ var _refund_refund_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./refund/refund.component */ 6173);
/* harmony import */ var _privacy_privacy_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./privacy/privacy.component */ 8159);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);









let ProfilePage = class ProfilePage {
    constructor(router, modalController) {
        this.router = router;
        this.modalController = modalController;
        this.user = JSON.parse(localStorage.getItem("loggedInUser"));
        console.log(this.user);
    }
    ngOnInit() {
    }
    signOut() {
        // localStorage.removeItem("loggedInUser");
        localStorage.clear();
        this.router.navigateByUrl('login');
    }
    openMyTerms() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _terms_terms_component__WEBPACK_IMPORTED_MODULE_2__.TermsComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    openMyRefund() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _refund_refund_component__WEBPACK_IMPORTED_MODULE_3__.RefundComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
    openMyPrivacy() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _privacy_privacy_component__WEBPACK_IMPORTED_MODULE_4__.PrivacyComponent,
                backdropDismiss: true,
                cssClass: 'popup-terms',
                swipeToClose: true,
            });
            return yield modal.present();
        });
    }
};
ProfilePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController }
];
ProfilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-profile',
        template: _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ProfilePage);



/***/ }),

/***/ 231:
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9maWxlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ 2907:
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar>\r\n    <!-- <ion-buttons slot=\"start\" routerLink=\"/home\">\r\n      <ion-icon name=\"chevron-back-outline\" ></ion-icon>\r\n    </ion-buttons> -->\r\n    <ion-title  slot=\"secondary\" >My Profile</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-badge routerLink=\"edit\"  class=\" badge editor-pf\">\r\n        <img class=\"\" src=\"../../assets/icon/Icon awesome-user-edit.svg\" alt=\"\">\r\n\r\n      </ion-badge>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding bg-image\">\r\n      <ion-grid>\r\n        <ion-row>\r\n          <ion-col size=\"12\" sizeLg=\"8\">\r\n            <div class=\"card-st\">\r\n              <ion-row class=\"ion-align-items-center\">\r\n                <ion-col size=\"3\" class=\"avatar-img\">\r\n                  <img class=\"user-avatar\" src=\"https://w7.pngwing.com/pngs/178/595/png-transparent-user-profile-computer-icons-login-user-avatars.png\">\r\n                </ion-col>\r\n                <ion-col size=\"9\">\r\n                  <div class=\"profile-info\">\r\n                    <b>{{user?.first_name}}&nbsp;{{user?.last_name}}  </b>\r\n                    <div class=\"light-color\">Beginner level</div>\r\n                    <div class=\"thin-color\">Wallet: <img src=\"../../assets/icon/Trading-3.svg\" alt=\"\"><span\r\n                        class=\"text-red\">999.99</span> </div>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n          \r\n              <div class=\"contact-info\">\r\n                <ion-grid>\r\n                  <ion-row  class=\"\">\r\n                    <ion-col size=\"4\">\r\n                      Phone Number\r\n                    </ion-col>\r\n                    <ion-col  size=\".5\"  class=\"ion-align-self-center\">\r\n                     :\r\n                    </ion-col>\r\n                    <ion-col class=\"ion-align-items-start\">\r\n                      <b>{{user?.phone_number}}</b> \r\n                    </ion-col>\r\n                  </ion-row>\r\n          \r\n                  <ion-row>\r\n                    <ion-col size=\"4\">\r\n                      Email\r\n                    </ion-col>\r\n                    <ion-col  size=\".5\">\r\n                      :\r\n                    </ion-col>\r\n                    <ion-col size=\"7.5\">\r\n                      <b style=\" width: 14em;\r\n                      overflow: hidden;\r\n                      display: -webkit-box;\r\n                      -webkit-line-clamp: 5;\r\n                      -webkit-box-orient: vertical;\">{{user?.email}}</b> \r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row>\r\n                    <ion-col size=\"4\">\r\n                      Date Of Birth\r\n                    </ion-col>\r\n                    <ion-col  size=\".5\">\r\n                      :\r\n                    </ion-col>\r\n                    <ion-col size=\"7.5\">\r\n                      <b>{{user?.dob}}</b> \r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row>\r\n                    <ion-col  size=\"4\">\r\n                      Country\r\n                    </ion-col>\r\n                    <ion-col  size=\".5\">\r\n                      :\r\n                    </ion-col>\r\n                    <ion-col size=\"7.5\" >\r\n                      <b>{{user?.country}}</b> \r\n\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </ion-grid>\r\n          \r\n              </div>\r\n              <div class=\"menu-bar\">\r\n                <ion-card>\r\n                  <ion-card-content>\r\n                    <b>Settings</b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n                <ion-card>\r\n                  <ion-card-content>\r\n                    <b>About Us</b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n                <ion-card>\r\n                  <ion-card-content (click)=\" openMyTerms()\">\r\n                    <b>Terms & consitions</b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n                <ion-card>\r\n                  <ion-card-content (click)=\"openMyPrivacy()\">\r\n                    <b>Privacy Policy</b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n                <ion-card>\r\n                  <ion-card-content (click)=\"openMyRefund()\">\r\n                    <b>Refund & Cancellation Policy</b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n                <ion-card>\r\n                  <ion-card-content (click)=\"signOut()\">\r\n                    <b><ion-label color=\"danger\">Log Out</ion-label></b>\r\n                  </ion-card-content>\r\n                </ion-card>\r\n              </div>\r\n          \r\n          \r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"12\" sizeLg=\"4\">\r\n\r\n            \r\n            <ion-card class=\"side-edit\" style=\"width: max-content; margin: 20% 0px;\">\r\n              <ion-card-content   routerLink=\"edit\" color=\"light\" class=\"ion-float-right\">\r\n                  <div class=\"editor-pf\"> <img src=\"../../assets/icon/Icon awesome-user-edit.svg\" alt=\"\"> <label for=\"\">Edit-Profile</label></div>\r\n            \r\n              </ion-card-content>\r\n            </ion-card>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n \r\n\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_profile_profile_module_ts.js.map