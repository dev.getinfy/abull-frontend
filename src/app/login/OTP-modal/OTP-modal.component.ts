import { AppHttpService } from './../../app-http.service';
import { Component, OnInit , Input} from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ABullApiService } from '../../a-bull-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-otp-modal',
  templateUrl: './OTP-modal.component.html',
  styleUrls: ['./OTP-modal.component.css']
})
export class OTPModalComponent implements OnInit {

  timeLeft = 30;
  interval;
  otpNumber:number;
  phone;
  loginUserData;
  resend = false
  constructor(public modalController: ModalController, public aBullservice: ABullApiService,public router: Router, public http: AppHttpService) {
     this.countdown();

  }

  ngOnInit() {
  }
  onchangeotp(e){
    console.log(e.detail.value);
    this.otpNumber = e.detail.value;
  }


  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
    clearInterval(this.interval);
  }

  onResendOTP() {
    if (this.phone) {
      let data = {
        phone: this.phone
      }
      this.aBullservice.aBullSignIn(data).subscribe(res => {
        if(res.status == 'success'){
        this.http.ShowToastMessage("OTP sent successfully", " ", "success")
        }
        if(res.status == 'failed'){
        this.http.ShowToastMessage("login failed", "Please try again later", "danger")
        }
 
      },err =>{
        this.http.ShowToastMessage("login failed", "Please try again later", "danger")
        console.log(err)
      })
    }
  }

  onSubmitOTP() {
    const data = {
      phone: this.phone,
      otp: this.otpNumber,
    };
    this.aBullservice.aBullVerifyOtp(data).subscribe(res => {
      if(res.status == 'success'){
        this.http.ShowToastMessage("login successfull", " ", "success");
         this.loginUserData = JSON.stringify(res.data);
      localStorage.setItem('token', JSON.parse(this.loginUserData).token);
      console.log(res);
      localStorage.setItem('loggedInUser', this.loginUserData);
      this.modalController.dismiss({
        dismissed: true
      });
      this.router.navigate(['app/tabs/home']);
        }
        if(res.status == 'failed'){
        this.http.ShowToastMessage("login failed", "Please try again later", "danger")
        }

      clearInterval(this.interval);

    },err =>{
      this.http.ShowToastMessage("login failed", "Please try again later", "danger")
    });
  }



  countdown() {
    let time = 30;
    this.interval = setInterval(() => {
      if (time === -1) {
        this.resend = true;
        return;
      } else {
        const stringVal = '00:' + time;
        document.getElementById('time').innerHTML = stringVal;
        console.log();
        time--;
      }
    }, 1000);





  }

}
