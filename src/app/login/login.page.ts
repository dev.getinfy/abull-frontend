import { Routes } from '@angular/router';
import { AppHttpService } from './../app-http.service';
import { ABullApiService } from './../a-bull-api.service';
import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';
// import { ModalPage } from '../modal/modal.page';
import { OTPModalComponent } from './OTP-modal/OTP-modal.component';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  phone;

  constructor(private authService : SocialAuthService, public modalController: ModalController, public aBullservice: ABullApiService, public http: AppHttpService) { }

  ngOnInit() {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: OTPModalComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  Onlogin() {
    if(!this.phone){
      this.http.ShowToastMessage("Enter Valid Phone Number", "Please try again later", "danger")
      return;
    }
    if (this.phone) {
      let data = {
        "phone": this.phone
      }
      this.aBullservice.aBullSignIn(data).subscribe(res => {
        if(res.status == 'success'){
        this.http.ShowToastMessage("OTP sent successfully", " ", "success")
        }
        if(res.status == 'failed'){
        this.http.ShowToastMessage("login failed", "Please try again later", "danger")
        }

        if (res.message == 'Otp sent') {
          this.openMyModal();
        }
      },err =>{
        this.http.ShowToastMessage("login failed", "Please try again later", "danger")
        console.log(err)
      })
    }
  }



  async openMyModal() {



    const modal = await this.modalController.create({

      component: OTPModalComponent,

      backdropDismiss: true,

      cssClass: 'options_modal',

      componentProps: { phone: this.phone }


    });

    return await modal.present();



  }


  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

}

