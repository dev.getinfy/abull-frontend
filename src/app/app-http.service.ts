import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController } from '@ionic/angular';

// RxJs
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AppHttpService {

  // public readonly API_MOCK_ENDPOINT:string = 'http://ec2-65-0-71-206.ap-south-1.compute.amazonaws.com:4220';
  
  public readonly API_MOCK_ENDPOINT:string = 'https://api.abull.app';
  constructor(private http: HttpClient, public toastController: ToastController) { }






public get = (url: string, options?: any): Observable<any> => this.http.get(url, options);

public post = (url: string, data: any, options?: any): Observable<any> => this.http.post(url, data, options);

public put = (url: string, data: any, options?: any): Observable<any> => this.http.put(url, data, options);

public delete = (url: string, options?: any): Observable<any> => this.http.delete(url, options);


public async ShowToastMessage(header, message, color: string = 'primary') {
  const toast = await this.toastController.create({
      header: header,
      message: message,
      position: 'top',  duration: 1500,
      color: color,
      cssClass:'my-toast-class'
   
  });
  toast.present();

}
}
