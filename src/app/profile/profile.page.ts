import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {TermsComponent} from './terms/terms.component';
import { RefundComponent } from './refund/refund.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ModalController } from '@ionic/angular';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
user
  constructor(private router: Router,public modalController: ModalController ) { 
    this.user = JSON.parse(localStorage.getItem("loggedInUser"));
    console.log(this.user);
  }

  ngOnInit() {
  }

  
  signOut() {
    // localStorage.removeItem("loggedInUser");
    localStorage.clear();
    this.router.navigateByUrl('login');

  }
  
  

  async openMyTerms() {



    const modal = await this.modalController.create({

      component: TermsComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }
  async openMyRefund() {



    const modal = await this.modalController.create({

      component: RefundComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }
  async openMyPrivacy() {



    const modal = await this.modalController.create({

      component: PrivacyComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }
}
