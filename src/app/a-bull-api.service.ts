import { Injectable } from '@angular/core';
import { AppHttpService } from './app-http.service';
@Injectable({
  providedIn: 'root'
})
export class ABullApiService {

  // public readonly aBull_Host: string = 'http://ec2-65-0-71-206.ap-south-1.compute.amazonaws.com:4220';
  public readonly aBull_Host: string = 'https://dev-api.abull.app';

  private readonly apiSignup: string = '/api/register';
  private readonly apiLogin: string = '/api/login';
  private readonly verifyOtp: string = '/api/verifyotp';
  private readonly registration: string = '/api/golden-trident-registration';
  private readonly contestDetails: string = '/api/golden-trident-contest';
  private readonly postsLists: string = '/api/posts-list';
  private readonly editProfile: string = '/api/edit-profile';
  private readonly postDetails: string = '/api/post-details';
 




  constructor(public httpService: AppHttpService,) { }
  public get tokenHeaders() {
    let token = localStorage.getItem("token");
    const headers = {
    'Content-Type': 'application/json',
    'x-access-token': token
    };
    return { headers: headers };
    }
  public aBullSignUp(info) {
    const path = this.aBull_Host + this.apiSignup;
    return this.httpService.post(path, info);
  };

  public aBullSignIn(info) {
    const path = this.aBull_Host + this.apiLogin;
    return this.httpService.post(path, info);
  };

  public aBullVerifyOtp(info) {
    const path = this.aBull_Host + this.verifyOtp;
    return this.httpService.post(path, info);
  };

  public aBUllGTCRegistration(info){
    let path = this.aBull_Host + this.registration;
    return this.httpService.post(path, info, this.tokenHeaders);
  }

  public aBullContestDetails() {
    const path = this.aBull_Host + this.contestDetails;
    return this.httpService.get(path, this.tokenHeaders);
  }

  public aBullPostLists(){
    const path = this.aBull_Host + this.postsLists;
    return this.httpService.get(path, this.tokenHeaders);
  }

  public aBullEditProfile(data){
    const path = this.aBull_Host + this.editProfile;
    return this.httpService.post(path, data, this.tokenHeaders)
  }

  public aBullPostDetails(postId){
    const path = this.aBull_Host + this.postDetails + '/' + postId;
    return this.httpService.get(path,this.tokenHeaders)
  }




}
