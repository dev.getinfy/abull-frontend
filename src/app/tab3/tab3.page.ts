import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ABullApiService } from './../a-bull-api.service';
import { TermsComponent } from '../profile/terms/terms.component';
import { ModalController } from '@ionic/angular';
import { PrivacyComponent } from '../profile/privacy/privacy.component';
import { RefundComponent } from '../profile/refund/refund.component';

import 'capacitor-razorpay';
import { Plugins } from '@capacitor/core';
import { AppHttpService } from '../app-http.service';
const { Checkout } = Plugins;

import { AllInOneSDKPlugin } from 'capacitor-paytm-allinone';
import { ApiUrls } from '../api-url';
import { CheckoutService } from 'paytm-blink-checkout-angular';
import { Subscription } from 'rxjs';

// import { Plugins } from "@capacitor/core";
const { AllInOneSDK } = Plugins;


declare var $: any;
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit, OnDestroy {


  isEnrolled;
  edu_institution;
  age_group;
  city;
  state;
  referral_code;
  terms;
  contestDetails;
  errorMsg;
  isEnabled;
  enableEnroll = false;
  details: any;
  gtcLevelData;
  private subs: Subscription;


  constructor(private readonly checkoutService: CheckoutService, public aBUllservice: ABullApiService, public Acroute: ActivatedRoute, public http: AppHttpService, public router: Router, public modalController: ModalController) {
    this.checkoutService.init(
      //config
      {
        data: {
          orderId: "test4",
          amount: "3337",
          token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxNDgsImlhdCI6MTY0MDAxMjA2MCwiZXhwIjoxNjQwMDE5MjYwfQ.F2ZjKkeFLSoYsGUWVqfLuTS126CZmjiFDFY8pk6_X9c",
          tokenType: "TXN_TOKEN"
        },
        merchant: {
          mid: "IDHzGYtY74775291070598",
          name: "Dummy",
          redirect: true
        },
        flow: "DEFAULT",
        handler: {
          notifyMerchant: this.notifyMerchantHandler
        }
      },
      //options
      {
        env: 'PROD', // optional, possible values : STAGE, PROD; default : PROD
        openInPopup: true // optional; default : true
      })

    this.subs = this.checkoutService
      .checkoutJsInstance$
      .subscribe(instance => console.log(instance));

    this.Acroute.params.subscribe(params => {
      this.resetVals();
    });
  }

  ngOnInit() {
    this.getaBUllGTCDetails();
    this.disablePayment();
    this.getGTCLevel();
  }

  notifyMerchantHandler = (eventType, data): void => {
    console.log('MERCHANT NOTIFY LOG', eventType, data);
  }

  


  getGTCLevel() {
    this.http.get(ApiUrls.gtcLevel).subscribe((res: any) => {
      if (res.status) {
        this.gtcLevelData = res.data;
        if(this.gtcLevelData.level === 'mcq'){
          this.router.navigate(['app/tabs/gtc/contest'])
        }
        if(this.gtcLevelData.level === 'laq'){
          this.router.navigate(['app/tabs/gtc/contest/step2'])
        }
        if(this.gtcLevelData.level === ''){
          this.router.navigate(['app/tabs/gtc'])
        }
       
      }
    })
  }


  async loadCheckout() {







    if (!this.contestDetails && !this.contestDetails.price) {
      console.warn("amount unavailable");
      return false;
    }
    const options = {
      key: 'rzp_live_ro1JaoBHiT5gz1',
      amount: this.contestDetails.price * 100,//paise
      description: 'Credits towards abull',
      image: 'https://abull.app/assets/images/aBull%20Logo%20new1.png',
      currency: 'INR',
      name: 'Golden Trident Contest',
      // prefill: {
      //   contact: '99999999999',
      //   name: 'Razorpay Software'
      // },
      theme: {
        color: '#3b5998'
      }
    }


    try {

      var userNumber = JSON.parse(localStorage.loggedInUser).phone_number;
      if (userNumber) {
        options["prefill"] = {
          contact: userNumber
        }
      }

    } catch (error) {
      console.warn(error, "phone failed");
    }

    try {
      let data = (await Checkout.open(options));
      console.log(data.response);
      if (data.response && data.response.razorpay_payment_id) {
        this.aBUllGTCRegistration(data.response.razorpay_payment_id);
      }

    } catch (error) {

      this.http.ShowToastMessage("Payment failed", "Please try again later", "danger")
      console.log(error.message, "payment failed"); //Doesn't appear at all
    }
  }




  aBUllGTCRegistration(payment_id) {

    try {
      this.details = JSON.parse(localStorage.getItem('gtsdetails'));
      console.log(this.details);
      if (this.details) {
        this.details["transaction_details"]["trans_id"] = payment_id;
        this.aBUllservice.aBUllGTCRegistration(this.details).subscribe(res => {
          this.http.ShowToastMessage("You have registered successfully", " ", "success");
          this.router.navigate(["/paysuccess"])

        });
      }

    } catch (error) {
      this.http.ShowToastMessage("Registration  failed", "Please contact admin", "danger")
    }


  }



  async paytm() {

    let paytm = {
      MID: "xxxxx", // paytm provide
      WEBSITE: "WEBSTAGING", // paytm provide
      INDUSTRY_TYPE_ID: "Retail", // paytm provide
      CHANNEL_ID: "WEB", // paytm provide
      ORDER_ID: "xxxxx", // unique id
      CUST_ID: "xxxxx", // customer id
      MOBILE_NO: "xxxx", // customer mobile number
      EMAIL: "xxxx", // customer email
      TXN_AMOUNT: "10.00", // transaction amount
      CALLBACK_URL: "http://localhost:4200/paymentverity", // Call back URL that i want to redirect after payment fail or success
    };

    this.http.post('https://api.abull.app/createchecksum', paytm)
      .subscribe((res: any) => {
        // As per my backend i will get checksumhash under res.data
        this.paytm['CHECKSUMHASH'] = res.data;
        // than i will create form
        this.createPaytmForm();
      });

  }

  createPaytmForm() {
    const my_form: any = document.createElement('form');
    my_form.name = 'paytm_form';
    my_form.method = 'post';
    my_form.action = 'https://securegw-stage.paytm.in/order/process';

    const myParams = Object.keys(this.paytm);
    for (let i = 0; i < myParams.length; i++) {
      const key = myParams[i];
      let my_tb: any = document.createElement('input');
      my_tb.type = 'hidden';
      my_tb.name = key;
      my_tb.value = this.paytm[key];
      my_form.appendChild(my_tb);
    };

    document.body.appendChild(my_form);
    my_form.submit();
    // after click will fire you will redirect to paytm payment page.
    // after complete or fail transaction you will redirect to your CALLBACK URL
  };



  resetVals() {
    this.errorMsg = '';
    this.edu_institution = '';
    this.age_group = '';
    this.referral_code = '';
    this.terms = false;
    this.state = '';
    this.city = '';

  }
  ngOnDestroy() {

    this.disablePayment();
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  validate() {
    this.disablePayment();
    this.errorMsg = '';
    if (!this.edu_institution) {
      this.errorMsg = 'please provide education institition';
      return;
    }
    if (!this.age_group) {
      this.errorMsg = 'please provide age group';
      return;
    }
    if (!this.referral_code) {
      this.errorMsg = 'please provide referal code';
      return;
    }
    if (!this.terms) {
      this.errorMsg = 'please accept our terms and condotions';
      return;
    }
    this.isEnabled = true; this.errorMsg = '';
    this.enablePayment();
    let data = {
      "gtc_id": this.contestDetails.id,
      "edu_institution": this.edu_institution,
      "age_group": this.age_group,
      "city": this.city,
      "state": this.state,
      "referral_code": this.referral_code,
      "is_terms_accepted": 1,
      "transaction_details": {
        "trans_status": "Success"
      }
    };

    localStorage.setItem('gtsdetails', JSON.stringify(data));
  }

  disablePayment() {
    this.enableEnroll = false;

    // $("#payment-btn-frame").addClass("invisible");
  }

  enablePayment() {

    this.enableEnroll = true;
    // $("#payment-btn-frame").removeClass("invisible")
  }


  getaBUllGTCDetails() {
    this.aBUllservice.aBullContestDetails().subscribe(res => {
      if (res && res.data) {
        this.contestDetails = res.data[0];
      }
      console.log(res, "details");

    });

  }

  // pop up T&C
  async openMyTerms() {



    const modal = await this.modalController.create({

      component: TermsComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }
  async openMyPrivacy() {



    const modal = await this.modalController.create({

      component: PrivacyComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }
  async openMyRefund() {



    const modal = await this.modalController.create({

      component: RefundComponent,

      backdropDismiss: true,

      cssClass: 'popup-terms',

      swipeToClose: true,


    });

    return await modal.present();



  }


}
