import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ABullApiService } from '../a-bull-api.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
   user;
  countries = ["India", "United States", "United Kingdom", "Australia", "United Arab Emirates", "China", "Canada", "Japan", "Germany"];

  constructor(private router: Router,  public aBullservice: ABullApiService ) { 
    this.user = JSON.parse(localStorage.getItem("loggedInUser"));
    console.log(this.user);
  }

  ngOnInit() {
  }

  onEditProfile(){
    let data ={
      
        "first_name": this.user.first_name,
        "last_name": this.user.last_name,
        "email": this.user.email,
        "phone_number": this.user.phone_number,
         "country": this.user.country,
         "dob": this.user.dob
    
    }
    this.aBullservice.aBullEditProfile(data).subscribe(res => {
     console.log(res,"...")
     localStorage.setItem('loggedInUser',JSON.stringify(data))
    })
    // console.log(this.user)
  }

}
