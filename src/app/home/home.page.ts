import { ABullApiService } from './../a-bull-api.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public postsList = [];
selctedPost;
  constructor(private aBull: ABullApiService,  private router: Router) { this.aBullPostLists(); }

  ngOnInit() {
  }

  aBullPostLists(){



  //   var responseEx =  {
  //     "status": "success",
  //     "data": [
  //         {
  //             "id": 1,
  //             "title": "First post",
  //             "description": "<p>Post Description</p>",
  //             "post_type": "Image",
  //             "file": "http://localhost/abull_admin/public/uploads/posts/8fXP64N6r2.jpg",
  //             "category_id": 1,
  //             "moderator_id": 2,
  //             "status": 1,
  //             "createdAt": "2021-10-26T01:25:57.000Z",
  //             "updatedAt": "2021-10-26T01:25:57.000Z",
  //             "deletedAt": null,
  //             "categoryId": 1,
  //             "ModeratorId": 2,
  //             "category": {
  //                 "id": 1,
  //                 "name": "Cat1",
  //                 "status": 1,
  //                 "sortorder": 1,
  //                 "createdAt": "2021-10-26T01:24:58.000Z",
  //                 "updatedAt": "2021-10-26T01:24:58.000Z",
  //                 "deletedAt": null
  //             },
  //             "Moderator": {
  //                 "id": 2,
  //                 "first_name": "Moderator",
  //                 "last_name": "1"
  //             }
  //         },
  //         {
  //             "id": 2,
  //             "title": "Post 2",
  //             "description": "<p>Post 2 Description</p>",
  //             "post_type": "Image",
  //             "file": "http://localhost/abull_admin/public/uploads/posts/xXof9axijF.jpg",
  //             "category_id": 2,
  //             "moderator_id": 2,
  //             "status": 1,
  //             "createdAt": "2021-10-26T01:26:28.000Z",
  //             "updatedAt": "2021-10-26T01:26:28.000Z",
  //             "deletedAt": null,
  //             "categoryId": 2,
  //             "ModeratorId": 2,
  //             "category": {
  //                 "id": 2,
  //                 "name": "cat2",
  //                 "status": 1,
  //                 "sortorder": 2,
  //                 "createdAt": "2021-10-26T02:35:47.000Z",
  //                 "updatedAt": "2021-10-26T02:35:47.000Z",
  //                 "deletedAt": null
  //             },
  //             "Moderator": {
  //                 "id": 2,
  //                 "first_name": "Moderator",
  //                 "last_name": "1"
  //             }
  //         }
  //     ],
  //     "message": "Posts List",
  //     "current_time": "2021-10-29T07:34:56.496Z"
  // };

    this.aBull.aBullPostLists().subscribe(res => {

      if(res.data){
        this.postsList  = res.data;
      }
      console.log(this.postsList);
    });
  }

  onPostClick(post){
    this.selctedPost = post;
    // console.log(this.selctedPost.id);
    this.router.navigate(['post-details/'+post.id])
  }


}
