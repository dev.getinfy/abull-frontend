import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModeratorDetailsPage } from './moderator-details.page';

const routes: Routes = [
  {
    path: '',
    component: ModeratorDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModeratorDetailsPageRoutingModule {}
