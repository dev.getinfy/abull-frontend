import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModeratorDetailsPageRoutingModule } from './moderator-details-routing.module';

import { ModeratorDetailsPage } from './moderator-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModeratorDetailsPageRoutingModule
  ],
  declarations: [ModeratorDetailsPage]
})
export class ModeratorDetailsPageModule {}
