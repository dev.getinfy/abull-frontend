import { HttpInterceptor, HttpEvent, HttpResponse, HttpRequest, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter, finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorsService {


  requestCount = 0;
  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let temp;
    const token = localStorage.getItem("token");
   

    if (token) {
      temp = req.clone({
        setHeaders: {
          'x-access-token': token,
          'token': token,
          'Content-Type': 'application/json;charset=utf-8'
        }})
      } else {
        temp = req;
      }


    if (this.requestCount === 0) {
        // this.loaderService.isLoading.next(true);
      }
      this.requestCount++;
      return next?.handle(temp).pipe(finalize(() => {
        this.requestCount--;
        if (this.requestCount === 0) {
          // hide loader
          // this.loaderService.isLoading.next(false);
        }
      }));
    }







    // intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //   let token;
    //   if(localStorage.getItem("token")){
    //     token = localStorage.getItem("token");
    //     return next.handle(httpRequest.clone({ setHeaders: { 
    //       'x-access-token' : token,
    //       'token' : token,
    //       'Content-Type': 'application/json',
    //       } }));
    //   }


    // }

  }
