import { environment } from "src/environments/environment";

const domain = environment.domain + '/api/';
export const ApiUrls = {
    postComment: domain + `post-comment`,
    comments : domain + `post-view-comments`,
    gtcLink : domain + `gtc-exam-link`,
    contest : domain + `golden-trident-contest`,
    leaderboard : domain + `leaderboard`,
    goodies : domain + `goodies`,
    gtcLevel : domain + `gtc-level`,
    mcq_result : domain + `mcq-exam-result`,
    laqExamSubmit : domain + `laq-exam-submit`
}
