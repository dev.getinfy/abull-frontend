import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiUrls } from 'src/app/api-url';
import { ContestService } from '../contest.service';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {

  step2Success = false;
  contestDetails;
  constructor(private contest: ContestService, private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.contest.getData().subscribe((res: any) => {
      if (res) {
        this.step2Success = res.step2
      }
    })
    // setTimeout(() => {
    //   this.router.navigate(['app/tabs/gtc/contest/step3']);
    //   this.contest.add({ step2: true, step1: true, step3: true })
    // }, 3000);
    this.getContestDetails();
  }


  getContestDetails() {
    this.http.get(ApiUrls.contest).subscribe((res: any) => {
      if (res.status) {
        this.contestDetails = res.data[0];
        if (this.contestDetails) {
          console.log(this.contestDetails.end_date)
          let countDownDate = new Date(this.contestDetails.end_date).getTime();
          let x = setInterval(function () {

            // Get todays date and time
            let now = new Date().getTime();

            // Find the distance between now and the count down date
            let distance = countDownDate - now;
            // Time calculations for days, hours, minutes and seconds
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // console.log(now, "now", "countDownDate", countDownDate, "distance", distance, "days", days);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
              + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text 
            if (distance < 0) {
              clearInterval(x);
              document.getElementById("demo").innerHTML = "EXPIRED";
            }
          }, 1000);
        }
      }
    })
  }



 
}
