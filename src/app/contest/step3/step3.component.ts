import { Component, OnInit } from '@angular/core';
import { ContestService } from '../contest.service';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.css']
})
export class Step3Component implements OnInit {

  step3Success = false
  constructor(private contest : ContestService) { }

  ngOnInit(): void {
    this.contest.getData().subscribe((res:any) => {
      if(res){
        this.step3Success = res.step3
      }
    })
  }

}
