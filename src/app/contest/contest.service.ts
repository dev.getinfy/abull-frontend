import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContestService {

  private data = new BehaviorSubject([]);
  constructor() { }


  add(item:any){
    this.data.next(item)
  }

  getData() {
    return this.data.asObservable();
  }



}
