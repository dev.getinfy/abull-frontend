import { Component, OnInit } from '@angular/core';
import  YoutubePlayer  from 'youtube-player';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  player:any;
  constructor() { }

  ngOnInit(): void {
    this.openVideo()
  }

  openVideo(){
    this.player = YoutubePlayer('video');
    this.player.
    this.player.loadVideoById('O4qq18_PFFI').then((res) => {
      this.player.playVideo()
    })
  }

  back(){
    window.history.back()
  }


}
