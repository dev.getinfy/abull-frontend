import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContestRoutingModule } from './contest-routing.module';
import { ContestComponent } from './contest.component';
import { HeaderComponent } from './header/header.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { GoodiesComponent } from './goodies/goodies.component';
import { CountdownModule } from 'ngx-countdown';
import { LeaderBoardComponent } from './leader-board/leader-board.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ContestComponent,
    HeaderComponent,
    Step1Component,
    Step2Component,
    Step3Component,
    GoodiesComponent,
    LeaderBoardComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ContestRoutingModule,
    CountdownModule
  ],
  providers : [
    YoutubeVideoPlayer
  ]
})
export class ContestModule { }
