import { Component, OnInit } from '@angular/core';
import { ContestService } from './contest.service';

@Component({
  selector: 'app-contest',
  templateUrl: './contest.component.html',
  styleUrls: ['./contest.component.css']
})
export class ContestComponent implements OnInit {

  firstSuccess = false;
  secondSuccess = false;
  thirdSuccess = false;
  constructor(private contest : ContestService) { }

  ngOnInit(): void {
    this.contest.getData().subscribe((res:any) => {
      if(res){
        console.log(res)
        this.firstSuccess = res.step1
        this.secondSuccess = res.step2
        this.thirdSuccess = res.step3
      }
    })
  }

}
