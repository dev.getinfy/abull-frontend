import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiUrls } from 'src/app/api-url';

@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.css']
})
export class LeaderBoardComponent implements OnInit {

  leaderBoard;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.getLeader()
  }
  back(){
    window.history.back()
  }


  getLeader(){
    
    this.http.get(ApiUrls.leaderboard).subscribe((res:any) => {
      if(res.status){
        this.leaderBoard = res.data;
      }
    })
  }

}
