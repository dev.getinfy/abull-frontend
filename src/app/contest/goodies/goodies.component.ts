import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ApiUrls } from 'src/app/api-url';

@Component({
  selector: 'app-goodies',
  templateUrl: './goodies.component.html',
  styleUrls: ['./goodies.component.css']
})
export class GoodiesComponent implements OnInit {

  goodies;
  constructor(private http : HttpClient) { }

  ngOnInit(): void {
    this.getGoodies()
  }

  back(){
    window.history.back()
  }

  getGoodies(){
    
    this.http.get(ApiUrls.goodies).subscribe((res:any) => {
      if(res.status){
        this.goodies = res.data;
      }
    })
  }

}
