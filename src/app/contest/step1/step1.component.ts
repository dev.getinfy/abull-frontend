import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Browser } from '@capacitor/browser';
import YoutubePlayer from 'youtube-player';


import { ABullApiService } from 'src/app/a-bull-api.service';
import { environment } from 'src/environments/environment';
import { ContestService } from '../contest.service';
import { ApiUrls } from 'src/app/api-url';
import { ToastController } from '@ionic/angular';




@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.css']
})
export class Step1Component implements OnInit {

  step1Success = false;
  userData;
  contestDetails;
  someDate;
  api = environment.domain
  player: any;
  gtcLink;
  openBrowser: any;
  laqresponse;
  question_details;

  constructor(private toastController: ToastController, private contest: ContestService, private router: Router, public aBullservice: ABullApiService, private http: HttpClient) {

  }

  ngOnInit(): void {

    this.someDate = new Date()
    this.userData = JSON.parse(localStorage.getItem('loggedInUser'))
    this.contest.getData().subscribe((res: any) => {
      if (res) {
        this.step1Success = res.step1
      }
    });
    this.getGTC();
    this.getContestDetails();

  }



  async openWeb() {
    this.openBrowser = await Browser.open({ url: `${this.gtcLink?.link}`, presentationStyle: 'popover' });

    setTimeout(() => {
      Browser.close();
      this.mcqResult()
    }, 5000);

    Browser.addListener('browserPageLoaded', () => {
      alert('Browser closed.')
    })
    this.contest.add({ step1: true });
  }





  submit() {

    let obj = {
      quiz_code : this.gtcLink?.quiz_code,
      laq_code : this.gtcLink?.quiz_code,
      laq_response : this.laqresponse
    }
    this.http.post(ApiUrls.laqExamSubmit, obj).subscribe((res: any) => {
      if (res.status) {
        console.log(res)
      }
    })

    this.contest.add({ step2: true, step1: true });
    this.router.navigate(['app/tabs/gtc/contest/step2'])
  }



  mcqResult() {
    this.http.post(ApiUrls.mcq_result, {
      quiz_code: this.gtcLink?.quiz_code
    }).subscribe((res: any) => {
      if (res) {
        console.log(res)
      }
    })
  }


  getGTC() {
    this.http.get(ApiUrls.gtcLink).subscribe((res: any) => {
      if (res.status === 'success') {
        this.router.navigate(['app/tabs/gtc/contest'])
        this.gtcLink = res.data;
        this.question_details = res.data?.question_details[0];
      } else {
        this.presentToast(res.message)
        this.router.navigate(['app/tabs/gtc/'])
      }
    })
  }

  getContestDetails() {
    this.http.get(ApiUrls.contest).subscribe((res: any) => {
      if (res.status) {
        this.contestDetails = res.data[0];
        if (this.contestDetails) {
          this.contestDetails.end_date = "2021-12-21"
          console.log(this.contestDetails.end_date)
          let countDownDate = new Date(this.contestDetails.end_date).getTime();
          let x = setInterval(function () {

            // Get todays date and time
            let now = new Date().getTime();

            // Find the distance between now and the count down date
            let distance = countDownDate - now;
            // Time calculations for days, hours, minutes and seconds
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // console.log(now, "now", "countDownDate", countDownDate, "distance", distance, "days", days);

            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
              + minutes + "m " + seconds + "s ";

            // If the count down is over, write some text 
            if (distance < 0) {
              clearInterval(x);
              document.getElementById("demo").innerHTML = "EXPIRED";
            }
          }, 1000);
        }
      }
    })
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }






}
