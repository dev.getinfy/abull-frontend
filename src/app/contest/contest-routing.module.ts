import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContestComponent } from './contest.component';
import { GoodiesComponent } from './goodies/goodies.component';
import { HeaderComponent } from './header/header.component';
import { LeaderBoardComponent } from './leader-board/leader-board.component';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { Step3Component } from './step3/step3.component';

const routes: Routes = [
  { 
    path: '', 
    component: ContestComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        redirectTo : 'step1'
      },
      {
        path : 'step1',
        component : Step1Component
      },
      {
        path : 'step2',
        component : Step2Component
      },
      {
        path : 'step3',
        component : Step3Component
      },
      
     
    ]
  },
  
  {
    path : 'goodies',
    component : GoodiesComponent
  },
  {
    path : 'leader-board',
    component : LeaderBoardComponent
  },

  {
    path : 'video/:id',
    component : HeaderComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContestRoutingModule { }
