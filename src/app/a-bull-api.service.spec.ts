import { TestBed } from '@angular/core/testing';

import { ABullApiService } from './a-bull-api.service';

describe('ABullApiService', () => {
  let service: ABullApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ABullApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
