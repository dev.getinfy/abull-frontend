import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ABullApiService } from '../a-bull-api.service'
import { ApiUrls } from '../api-url';
import { Share } from '@capacitor/share';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.page.html',
  styleUrls: ['./post-details.page.scss'],
})
export class PostDetailsPage implements OnInit {
  postId;
  postDetails;
  comment;
  childComment;
  showComments = true;
  comments = []
  constructor(public toastController : ToastController,private http: HttpClient, private aBull: ABullApiService, private activatedRoute: ActivatedRoute) { }
  

  ngOnInit() {
    this.postId = this.activatedRoute.snapshot.paramMap.get('id');
    // console.log(this.postId, "postId");
    this.getPostdetails(this.postId);
    this.getViewComments()
  }

  getPostdetails(postId) {
    this.aBull.aBullPostDetails(this.postId).subscribe(res => {
      console.log(res, "res");
      this.postDetails = res.data;
      console.log(this.postDetails, "postDetails");
    })
  }

  getViewComments() {
    this.http.get(ApiUrls.comments + `/${this.postId}`).subscribe((res: any) => {
      console.log(res)
      if(res.status){
        this.comments = res.data
      }
    
    })
  }


  postComment(item){
    console.log(item)
    let data:any = {}
    data.post_id = this.postId;
    data.parent_id = this.childComment ? `${item.id}` : "0";
    data.comment =  this.comment || this.childComment;

    this.http.post(ApiUrls.postComment, data).subscribe((res:any) => {
      if(res.status){
        this.presentToast(res.message)
        this.getViewComments();
        this.showComments = true;
        
      }
    })
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message:  message,
      duration: 2000,
      color : 'success'
    });
    toast.present();
  }

async share(){
  let url = window.location.href;
  await Share.share({
    title: `${this.postDetails?.title}`,
    url: `${url}`,
    dialogTitle: 'Share with buddies',
  });
}

}
