import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private Acroute: Router) {
    this.Acroute.events.subscribe(params => {
    this.disablePayment();
    })
  }
  disablePayment(){
    $("#payment-btn-frame").addClass("invisible");
  }
}
