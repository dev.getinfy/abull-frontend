import { AppHttpService } from './../app-http.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import{ABullApiService} from '../a-bull-api.service';

import { ModalController } from '@ionic/angular';
// import { ModalPage } from '../modal/modal.page';
import { OTPModalComponent } from '../login/OTP-modal/OTP-modal.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm : FormGroup;
  firstName: string;
  lastName: string;
  mobileNumber:string;
  userEmail: string;
  selectedCountry: string;
  dateOfBirth: string;
  referral_code: string;
  CurrentDate;
  terms= false;

  countries = ["India", "United States", "United Kingdom", "Australia", "United Arab Emirates", "China", "Canada", "Japan", "Germany"];



  constructor(public modalController: ModalController, public aBullApi :ABullApiService,public http: AppHttpService ) { }

  ngOnInit() {
  }

  signUp(){
    if(!this.terms){
      this.http.ShowToastMessage("terms and conditions", "are required", "danger")
      return;
    }
    let user = {
      first_name: this.firstName,
      last_name: this.lastName,
      phone_number: this.mobileNumber,
      email: this.userEmail,
      country: this.selectedCountry,
      dob: this.dateOfBirth,
      referral_code: "CODE"
    }
    console.log(user);
    this.aBullApi.aBullSignUp(user).subscribe(data => {
      if(data.status == 'success'){
        this.http.ShowToastMessage("registered succesfully", " ", "success")
        this.openMyModal()
        }
        if(data.status == 'failed'){
        this.http.ShowToastMessage(data.message, "danger")
        }
      console.log(data);
    },err =>{
      this.http.ShowToastMessage("register failed", "Please try again later", "danger")
    })


  }
  async openMyModal() {



    const modal = await this.modalController.create({

      component: OTPModalComponent,

      backdropDismiss: true,

      cssClass: 'options_modal',

      componentProps: { phone: this.mobileNumber }


    });

    return await modal.present();



  }

}
