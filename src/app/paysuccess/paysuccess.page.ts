import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ABullApiService } from './../a-bull-api.service';

@Component({
  selector: 'app-paysuccess',
  templateUrl: './paysuccess.page.html',
  styleUrls: ['./paysuccess.page.scss'],
})
export class PaysuccessPage implements OnInit {
  contestDetails: any;

  details: any;
  constructor(private router : Router, public aBull: ABullApiService, public route: ActivatedRoute,public aBUllservice: ABullApiService) { }

  ngOnInit() {
    //this.aBUllGTCRegistration();
    this.getaBUllGTCDetails();

    setTimeout(() => {
      this.router.navigate(['app/tabs/gtc/contest'])
    }, 2000);
  }



  aBUllGTCRegistration() {

    try {
      
    this.details = JSON.parse(localStorage.getItem('gtsdetails'));
    this.route.queryParamMap.subscribe((params) => {
      console.log(this.details);
      if (this.details) {
        if (params['params']['payment_id']){
          this.details["transaction_details"]["trans_id"] = params['params']['payment_id'];
        }
           this.aBull.aBUllGTCRegistration(this.details).subscribe(res => {
                console.log(res);
              });
      }
    }
    );
    } catch (error) {
      
      
    }


  }



  getaBUllGTCDetails() {
    this.aBUllservice.aBullContestDetails().subscribe(res => {
      if(res && res.data){
      this.contestDetails = res.data[0];
    }
      console.log(res );

    });

  }
}
